3M.C.ACID;3-METHYLCROTONIC ACIDURIA;NONE;NO_ICD
ACRODERMA;ACRODERMATITIS ENTEROPATHICA;E83.2;Disorders of zinc metabolism
ALDOLASE;ALDOLASE B DEFICIENCY;NONE;NO_ICD
ALKAPTON;ALKAPTONURIA;E70.2;Disorders of tyrosine metabolism
ARGININEMIA;ARGININEMIA;NONE;E72.2
ASA;ARGININOSUCCINIC ACIDURIA;E72.2;Disorders of urea cycle metabolism
APKU;ATYPICAL  PHENYLKETONURIA;NONE;NO_ICD
BIOT.DEF;BIOTINIDASE DEFICIENCY;E53.8;Deficiency of other specified B group vitamins
BHG;BHG;NONE;NO_ICD
BUT.ACID;BUTIRIK ACIDURIA;NONE;NO_ICD
CLA;CONGENITAL LACTIC ACIDOSIS;NONE;NO_ICD
CPS;CARBAMYL PHOSPHATE SYNTHETASE DEFICIENCY;NONE;NO_ICD
CYSTINOSIS;CYSTINOSIS;E72.0;Disorders of amino-acid transport
CYSTINURIA;CYSTINURIA;E72.0;Disorders of amino-acid transport
CREATIN;CREATIN DEFICIENCY;NONE;NO_ICD
COX DEF;COX DEFICIENCY;NONE;NO_ICD
CROUZON;CROUZON SYNDROME;Q75.1;Craniofacial dysostosis
DHPR;DIHYDROPTERIDINE REDUCTASE DEFICIENCY;NONE;NO_ICD
F1,6D DEF;FRUCTOSE-1,6-DIPHOSPHATASE DEFICIENCY;E74.1;Disorders of fructose metabolism
FARBER;FARBER DISEASE;E75.2;Other sphingolipidosis
FPPH;FPPH;NONE;NO_ICD
GALACTOSEM;GALACTOSEMIA;E74.2;Disorders of galactose metabolism
GLUT-1;GLUCOSE TRANSPORTER PROTEIN TYPE 1 DEFICIENCY;NONE;NO_ICD
GLUT-2;GLUCOSE TRANSPORTER PROTEIN TYPE 2 DEFICIENCY;NONE;NO_ICD
GLUT.ACID;GLUTARIK ASIDURI;NONE;NO_ICD
GSD;GLUTATION SYNTHETASE DEFICIENCY;NONE;NO_ICD
GOLDENHAR;GOLDENHAR SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
HOLOCARB.;HOLOCARBOXYLASE DEFICIENCY;NONE;NO_ICD
HBL;HYPO-β-LIPOPROTEINEMIA;NONE;NO_ICD
HFI;HEREDITARY FRUCTOSE INTOLERANCE;E74.1;Disorders of fructose metabolism
HMG COA;HMG COA LYASE DEFICIENCY;NONE;NO_ICD
H.SPATS;H.SPATS;NONE;NO_ICD
HYPERCHOL.;HYPERCHOLESTEROLEMIA;E78.0;Pure hypercholesterolaemia
H.AMMONEM.;HYPERAMMONEMIA;E72.2;Disorders of urea cycle metabolism
HYPERORNIT;HYPERORNITHINEMIA;NONE;NO_ICD
HYPERLIPID;HYPERLIPIDEMIA;E78;Disorders of lipoprotein metabolism and other lipidaemias
HYPERLYSIN;HYPERLYSINEMIA;E72.3;Disorders of lysine and hydroxylysine metabolism
FHCM;FAMILIAL HYPERCHYLOMICRONEMIA;E78;Disorders of lipoprotein metabolism and other lipidaemias
HYPOMAGNES;HYPOMAGNESEMIA;E83.4;Disorders of magnesium metabolism
HOMOCYS.;HOMOCYSTINURIA;E72.1;Disorders of sulfur-bearing amino-acid metabolism
HTG;HYPERTRIGLYCERIDEMIA;E78.1;Pure hyperglyceridaemia
HURLER;HURLER SYNDROME;E76.0;Mucopolysaccharidosis, type I
IFS;IDIOPATHIC FANCONI SYNDROME;NONE;NO_ICD
IG;IMMUNOGLYSINURIA;NONE;NO_ICD
IVA;ISOVALERIC ACIDEMIA;E71.1;Other disorders of branched-chain amino-acid metabolism
CARNITINE;CARNITINE DEFICIENCY;E71.3;Disorders of fatty-acid metabolism
KETOLYSIS;KETOLYSIS DEFECT;NONE;NO_ICD
KETOTHIO.;β-KETOTHIOLASE DEFICIENCY;NONE;NO_ICD
L.NYHAN;LESCH-NYHAN SYNDROME;E79.1;Lesch-Nyhan syndrome
LCAD;LCAD DEFICIENCY;NONE;E71.3
LPI;LYSINURIC PROTEIN INTOLERANCE;E72.3;Disorders of lysine and hydroxylysine metabolism
LPL DEF;LIPOPROTEIN LIPASE DEFICIENCY;E78;Disorders of lipoprotein metabolism and other lipidaemias
MA-MN;MALABSORBTION-MALNUTRITION;NONE;NO_ICD
MANNOSID.;α-MANNOSIDASE;NONE;NO_ICD
MCAD;MEDIUM-CHAIN DICARBOXYLIC ACIDURIA DEFICIENCY;NONE;NO_ICD
M.C.DEF;MULTIPLE CARBOXYLASE DEFICIENCY;NONE;NO_ICD
METHEMOG.;METHEMOGLOBULINEMI;NONE;NO_ICD
3 MGH;3 MGH DEFICIENCY;NONE;NO_ICD
MUCOLIPID;MUCOLIPIDOSIS;E77.0;Defects in post-translational modification of lysosomal enzymes
EMA;ETIL MALONIK ACIDEMIA;NONE;NO_ICD
MMA;METHYLMALONIC ACIDEMIA;E71.1;Other disorders of branched-chain amino-acid metabolism
MORQUIE;MORQUIE SYNDROME;NONE;NO_ICD
MPS;MUCOPOLYSACCHORIDOSIS;NONE;NO_ICD
MSUD;MAPLE SYRUP URINE DISEASE;E71.0;Maple-syrup-urine disease
NKH;NONKETOTIC HYPERGLYCINEMIA;NONE;NO_ICD
OBESITY;OBESITY;E66;Obesity
OTC;ORNITHINE TRANSCARBAMYLASE DEFICIENCY;E72.4;Disorders of ornithine metabolism
PDD;PYRUVATE DEHYROGENASE DEFICIENCY;NONE;NO_ICD
PEARSON;PEARSON;NONE;NO_ICD
PEROXISOM;PEROXISOMAL DISEASE;E80.3;Defects of catalase and peroxidase
PKU;PHENYLKETONURIA;E70.0;Classical phenylketonuria
PLA;PRIMER LACTIC ACIDOSIS;NONE;NO_ICD
PROTEINURI;PROTEINURIA;R80;Isolated proteinuria
PHA;PSEUDOHYPOALDOSTERONISM;N25.8;Other disorders resulting from impaired renal tubular function
P.C.DEF;PYRUVATE CARBOXYLASE DEFICIENCY;E74.4;Disorders of pyruvate metabolism and gluconeogenesis
PROLIDASE;PROLIDASE DEFICIENCY;NONE;NO_ICD
PROP.ACID;PROPIONIC ACIDEMIA;E71.1;Other disorders of branched-chain amino-acid metabolism
PTPS;PTPS DEFICIENCY;NONE;NO_ICD
RMC DEF;RR-MULTIPLE ACIL CoA DEHIDROGENASE DEFICIENCY;NONE;NO_ICD
CITRULLIN.;CITRULLINEMIA;E72.2;Disorders of urea cycle metabolism
SENGER;SENGER SYNDROME;NONE;NO_ICD
SOD;SULPHITE OXIDASE DEFICIENCY;NONE;Q04.8
SSADH;SUCCINIC SEMIALDEHYDE DEHYDROGENASE DEFICIENCY;NONE;NO_ICD
SUCLA 2;SUCLA 2 MUTATION;NONE;NO_ICD
TRANSCOBAL;TRANCOBALAMINE II DEFICIENCY;NONE;NO_ICD
TYROSIN.;TYROSINEMIA;E70.2;Disorders of tyrosine metabolism
UCD;UREA CYCLE DEFECT;E72.2;Disorders of urea cycle metabolism
VIT E DEF;VITAMIN E DEFICIENCY;E56.0;Deficiency of vitamin E
SERINE BD;DEFECTS OF SERINE BYOSYNTHESIS;NONE;NO_ICD
FAOD;FATTY ACID OXIDATION DISORDERS;NONE;NO_ICD
MOCOD;MOLYBDENUM COFACTOR DEFICIENCY;NONE;E72.1
ZELLWEGER;ZELLWEGER SYNDROME;Q87.8;Other specified congenital malformation syndromes, not elsewhere classified
ALAGILLE;ALAGILLE SYNDROME;Q44.7;Other congenital malformations of liver
ALPHA1-AD;ALPHA1-ANTITRYPSIN DEFICIENCY;NONE;NO_ICD
ARC;ARC SYNDROME;NONE;NO_ICD
BERARDIN S;BERARDINELLI SYNDROME;NONE;NO_ICD
BILIER;BILIER HYPOPLASIA;NONE;NO_ICD
BYLER;BYLER’S DISEASE;NONE;NO_ICD
CAROLI;CAROLI’S DISEASE;NONE;NO_ICD
CHAN.DORF.;CHANARIN-DORFMANN(NEUTRAL LIPID STORAGE DISEASE);NONE;NO_ICD
CHF;CONGENITAL HEPATIC FIBROSIS;NONE;NO_ICD
CHOLESTERO;CHOLESTERYL ESTER STORAGE DISEASE;E75.5;Other lipid storage disorders
CHRONIC DIARE;KRONIK ISHAL;NONE;NO_ICD
C.PANCREAT;CHRONIC PANCREATITIS;K86.0;Alcohol-induced chronic pancreatitis
CLD;CHRONIC LIVER DISEASE;NONE;NO_ICD
CRIG.NAJ.;CRIGLER-NAJJAR SYNDROME;E80.5;Crigler-Najjar syndrome
CROHN;INFANTIL CROHN;NONE;NO_ICD
DIARE;DIARE;NONE;NO_ICD
F-BICKELL;FANCONI-BICKELL SYNDROME;NONE;NO_ICD
F.CIRRHO;FAMILIAL CIRRHOSIS;NONE;NO_ICD
G-G M;GLUCOSE-GALACTOSE MALABSORPTION;E74.3;Other disorders of intestinal carbohydrate absorption
GALACTOSIAL;GALACTOSIALIDOSIS;E77.1;Defects in glycoprotein degradation
GAUCHER;GAUCHER’S DISEASE;E75.2;Other sphingolipidosis
GLY.STR.D;GLYCOGEN STORAGE DISEASE ( GLIKOJEN DEPO );NONE;NO_ICD
H.CHOMA;HEPATIC CHOMA;NONE;NO_ICD
MID;MICROVILLUS INCLUSION DISEASE;NONE;NO_ICD
H.CERULOP;FAMILIAL HYPOCERULOPLASMINEMIA;NONE;NO_ICD
EPH;EXTRAHEPATIC PORTAL HYPERTENSION;NONE;NO_ICD
ID.HEP;IDIOPATHIC HEPATITIS;NONE;NO_ICD
LSD;LIPID STORAGE DISEASE;E75;Disorders of sphingolipid metabolism and other lipid storage disorders
NC;NEONATAL CHOLESTASIS;NONE;NO_ICD
NIEM.PICK;NIEMANN PICK DISEASE;E75.2;Other sphingolipidosis
MUCOLIPID.;MUCOLIPIDOSIS;E77.0;Defects in post-translational modification of lysosomal enzymes
O-W-R;OSLER-WEBER-RENDU SYNDROME;I78.0;Hereditary haemorrhagic telangiectasia
PFIC;PROGRESSIVE FAMILIAL INTRAHEPATIC CHOLESTASIS;NONE;K83.1
WILSON;WILSON’S DISEASE;E83.0;Disorders of copper metabolism
ALP;FAMILIAL NEPHRITIS(ALPORT);NONE;NO_ICD
AKD;ATYPIC KAWASAKI DISEASE;NONE;NO_ICD
AMYLOID;AMYLOIDOSIS;E85;Amyloidosis
ANKILOZON;ANKILOZON SPONDILITIS;NONE;NO_ICD
ARTRIT;ARTRIT;NONE;NO_ICD
BA.B;BARDETT BIEDL SYNDROME;NONE;NO_ICD
BARTTER;BARTTER SYNDROME;E26.8;Other hyperaldosteronism
BEHCET;BEHCET’S DISEASE;M35.2;Beh�et disease
BRH;BILATERAL RENAL HYPOPLASIA;NONE;NO_ICD
CIAS I;CIAS I;NONE;NO_ICD
C3D;C3 DEFICIENCY;NONE;NO_ICD
CINKO/NOMID;CINKO/NOMID;NONE;NO_ICD
CITP;CONGENITAL INSENSITIVITY TO PAIN;NONE;NO_ICD
CNS;CONGENITAL NEPHROTIC SYNDROME;N04;Nephrotic syndrome
C1Q DEF;C1Q DEFICIENCY;NONE;NO_ICD
DENTS;DENT’S SYNDROME;NONE;NO_ICD
DMS;DIFFUSE MESANGIAL SCLEROSIS;NONE;NO_ICD
DRTA;DISTAL RENAL TUBULAR ACIDOSIS;NONE;N25.8
EHT;ESENTIAL HYPERTENSION;NONE;NO_ICD
F.NEPHRIT;FAMILIAL NEPHRITIS;NONE;NO_ICD
FGS;FOCAL GLOMERULOSCLEROSIS;NONE;NO_ICD
FSGS;FSGS;N00;Acute nephritic syndrome
FMF;FAMILIAL MEDITERRANEAN FEVER;E85.0;Non-neuropathic heredofamilial amyloidosis
FOAS;FACIO OCULA ACUSTICORENAL SYNDROME;NONE;NO_ICD
FRASIER;FRASIER SYNDROME;NONE;NO_ICD
GITELMAN;GITELMAN SYNDROME;N25.8;Other disorders resulting from impaired renal tubular function
GLICOSURIA;GLICOSURIA;NONE;NO_ICD
IGA NEPH.;IGA NEPHROPATHY;N02.8;Recurrent and persistent haematuria: Other
JRA;JUVENILE RHEUMATOID ARTHRITIS;M08.0;Juvenile rheumatoid arthritis
JUV.NEPHRO;JUVENILE NEPHRONOPHITISIS;NONE;NO_ICD
CFG;COLLAPSING FOCAL GLOMERULOSCLEROSIS;NONE;NO_ICD
LOWE;LOWE SYNDROME;E72.0;Disorders of amino-acid transport
MPGN;FAMILIAL NEPHRITIS (MPGN);NONE;NO_ICD
MULT.EXOS;MULTIPLE EXOSITOSIS;NONE;NO_ICD
NS;NEPHROTIC SYNDROME;N04;Nephrotic syndrome
ODPBH;ODPBH;NONE;NO_ICD
OFDS;OROFACIAL DIGITAL SYNDROME;NONE;NO_ICD
OXALOSIS;PRIMER OXALOSIS (HYPEROXALURIA TYPE 1);NONE;NO_ICD
PAN;POLYARTERITIS NODOSA;M30.0;Polyarteritis nodosa
PKD;POLYCYSTIC KIDNEY DISEASE;Q61;Cystic kidney disease
PEARSON;PEARSON SYNDROME;NONE;NO_ICD
RFS;RENAL FANCONI SYNDROME;NONE;NO_ICD
RTA;RENAL TUBULAR ACIDOSIS;N25.8;Other disorders resulting from impaired renal tubular function
SCLERODERMA;SCLERODERMA;L94.0;Localized scleroderma [morphea]
SEN-LEU S;SENIOR-LEUKEN SYNDROME;NONE;NO_ICD
SLE;SYSTEMIC LUPUS ERYTHEMATOSUS;M32;Systemic lupus erythematosus
ST.RES;STEROID RESISTANCE NEPHROTIC SYNDROME;NONE;NO_ICD
TMD;THIN MEMBRANE DISEASE;NONE;NO_ICD
TRAPS;TRAPS SYNDROME;NONE;NO_ICD
TS;TUBEROUS SCLEROSIS;Q85.1;Tuberous sclerosis
ADREN.S;ADRENOGENITAL SYNDROME;E25.0;Congenital adrenogenital disorders associated with enzyme deficiency
AROMATASE;AROMATASE DEFICIENCY;NONE;NO_ICD
C11BH DEF;C11 β-HYDROXYLASE DEFICIENCY;NONE;NO_ICD
CAH;CONGENITAL ADRENAL HYPERPLASIA;E25.0;Congenital adrenogenital disorders associated with enzyme deficiency
DIABET.INS;DIABETES INSIPIDUS;E23.2;Diabetes insipidus
DIDMOAD;DIDMOAD SYNDROME;NONE;NO_ICD
F.G.H.DEF;FAMILIAL GROWTH HORMONE DEFICIENCY;NONE;NO_ICD
F.GLUC.DEF;FAMILIAL GLUCOCORTICOID DEFICIENCY;NONE;NO_ICD
F.HYPOPARA;FAMILIAL HYPOPARATHYROIDISM;NONE;NO_ICD
F.I.TSH;FAMILIAL ISOLATED TSH DEFICIENCY;NONE;NO_ICD
FP;FEMALE PSEUDOHERMAPHRODITISM;NONE;NO_ICD
FAM.PANHIP;FAMILIAL PANHYPOPITUITARISM;NONE;NO_ICD
FERRITIN;FERRITIN;NONE;NO_ICD
FHH;FAMILIAL HYPOCALSIURIC HYPERCALSEMIA;NONE;E83.5
FMHH DEF;FAMILIAL MULTIPLE HYPOPHIZER HORMONE DEFICIENCY;NONE;NO_ICD
GRAVES;GRAVES DISEASE;E05.0;Thyrotoxicosis with diffuse goitre
GRH;GLUCOCORTICOID REMEDIABLE HYPERTENSION;NONE;NO_ICD
GSH;GLUCOCORTICOID SUPRESSOR HYPERTENSION;NONE;NO_ICD
HYPERINS;HYPERINSULINISM;E15;Nondiabetic hypoglycaemic coma
HYPOCHOND.;HYPOCHONDROPLASIA;Q77.4;Achondroplasia
HYPOTHYRO.;HYPOTHYROIDISM;E03.9;Hypothyroidism, unspecified
I.GLUC.DEF;ISOLATED GLUCOCORTICOID DEFICIENCY;NONE;NO_ICD
LAH;LIPOID ADRENAL HYPOPLASIA;NONE;NO_ICD
LERIWEILL;LERI WEILL SYNDROME;Q77.8;Other osteochondrodysplasia with defects of growth of tubular bones and spine
LIPOATR DM;LIPOATROPHIC DIABETES MELLITUS;NONE;NO_ICD
LRN;LARON TYPE DWARFISM;NONE;NO_ICD
MC CUNE AL;MC CUNE ALBRIGHT SYNDROME;NONE;NO_ICD
MODY;MODY TYPE DIABET;NONE;NO_ICD
P.E.TYPE 2;POLYGLANDULAR ENDOCRINOPATHY TYPE 2;NONE;NO_ICD
PS.HPT;PSEUDOHYPOPARATHYROIDISM;E20.1;Pseudohypoparathyroidism
RICKETS;RICKETS;E55;Vitamin D deficiency
T.TOCSIC;TESTOTOCSICOSIS;NONE;NO_ICD
TEST.FEM;TESTICULAR FEMINIZATION;E34.5;Androgen resistance syndrome
THR;THYROID HORMONE  RESISTANCE;E07;Other disorders of thyroid
TYPE 1 DM;TYPE 1 DIABETES MELLITUS;E10;Insulin-dependent diabetes mellitus
TRUE HER.;TRUE HERMAPHRODITISM;NONE;NO_ICD
F.LYMPHO.;FAMILIAL LYMPHODOME;NONE;NO_ICD
APERT;APERT SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
BALCI;BALCI SYNDROME;NONE;NO_ICD
BECKWITH;BECKWITH SYNDROME;NONE;NO_ICD
BLOOM;BLOOM SYNDROME;Q82.8;Other specified congenital malformations of skin
C-H SYND.;CONRADI-HUNERMANN SYNDROME;Q77.3;Chondrodysplasia punctata
CARPENTER;CARPENTER SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
CCD;CLEIDO CRANIAL DYSPLASIA;NONE;NO_ICD
COHEN;COHEN SYNDROME;NONE;NO_ICD
COIF;CONGENITAL ONYCHODYSPLASIA OF INDEX FINGER;NONE;Q84.6
CD;CONGENITAL DYSKERATOSIS;NONE;NO_ICD
D.INSIPIDUS;DIABETES INSIPIDUS;E23.2;Diabetes insipidus
FOKOMELI;FOKOMELI;NONE;NO_ICD
GREBE;GREBE SYNDROME;NONE;NO_ICD
GOLDENHAR;GOLDENHAR SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
HFOA;HEREDO FAMILIAL OPTIC ATROPHY;NONE;NO_ICD
I.VULGARIS;ICHTHYOSIS VULGARIS;Q80.0;Ichthyosis vulgaris
DOWN;DOWN SYNDROME;Q90;Down syndrome
IPF;IDIOPATHIC PULMONER FIBROSIS;NONE;NO_ICD
C.ANOMALI;CHROMOSOMAL ANOMALIES;NONE;NO_ICD
CHH;CARTILAGE HAIR HYPOPLASIA;NONE;NO_ICD
L-M-B;LAURENCE-MOON-BIEDL SYNDROME;Q87.8;Other specified congenital malformation syndromes, not elsewhere classified
LANG.MES;LANGER TYPE MESOMELIC DWARFISM;NONE;NO_ICD
LARSEN;LARSEN SYNDROME;NONE;NO_ICD
MOHR;MOHR SYNDROME;NONE;NO_ICD
M.GRUBER;MECKEL-GRUBER SYNDROME;Q61.9;Cystic kidney disease, unspecified
N.PATELLA;NAIL PATELLA SYNDROME;Q87.2;Congenital malformation syndromes predominantly involving limbs
OSTEOGENES;OSTEOGENESIS;NONE;NO_ICD
POLAND;POLAND SYNDROME;Q79.8;Other congenital malformations of musculoskeletal system
PROTEUS;PROTEUS SYNDROME;NONE;NO_ICD
R.HYPOPLAS;RADIAL HYPOPLASIA;NONE;NO_ICD
RING 4;RING 4;NONE;NO_ICD
ROBINOW;ROBINOW SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
RUBISTAYN;RUBISTAYN-TAYBI SYNDROME;NONE;NO_ICD
S.I.T.;SITUS INVERSUS TOTALIS;Q89.3;Situs inversus
SECKEL;SECKEL SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
SEDT-PA;SEDT-PA;NONE;NO_ICD
T.COLLINS;TREACHER COLLINS SYNDROME;Q75.4;Mandibulofacial dysostosis
T.N.D;TWENTY NAIL DYSPLASIA;NONE;NO_ICD
APDS;AUTO-PALATO-DIGITAL SYNDROME;NONE;NO_ICD
TD;THANATOPHORIC DYSPLASIA;Q77.1;Thanatophoric short stature
T.R.E.DEF;TETRAHYDROFOLATE REDUCTASE ENZYME DEFICIENCY;NONE;NO_ICD
XXXXY;XXXXY;NONE;NO_ICD
VACTER;VACTER SYNDROME;Q87.2;Congenital malformation syndromes predominantly involving limbs
VHLS;VON HIPPEL-LINDAU SYNDROME;Q85.8;Other phakomatoses, not elsewhere classified
WERNER;WERNER SYNDROME;NONE;NO_ICD
WILLIAMS;WILLIAMS SYNDROME;Q93.8;Other deletions from the autosomes
COFS;CEREBRO-OCULO-FACIO-SKELETAL SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
WPW;WPW SYNDROME;I45.6;Pre-excitation syndrome
ADA DEF;ADENOSINE DEAMINASE DEFICIENCY;D81.3;Adenosine deaminase [ADA] deficiency
ALPS;AUTOIMMUNE LYMPHOPROLIFERATIVE SYNDROME;NONE;D72.8
AT;ATAXIA-TELENGIECTASIA;NONE;NO_ICD
B CELL DEFICIENCY;B CELL DEF;D80;Immunodeficiency with predominantly antibody defects
BRUTON;BRUTON’S DISEASE;NONE;NO_ICD
C1 EST INH;C1 ESTERASE INHIBITOR DEFICIENCY;NONE;NO_ICD
C3 DEF;C3 DEFICIENCY;NONE;NO_ICD
C5 DEF;C5 DEFICIENCY;NONE;NO_ICD
CAR;CAR;NONE;NO_ICD
CD4 DEF;CD4 DEFICIENCY;NONE;NO_ICD
CGD;CHRONIC GRANULOMATOUS DISEASE;D71;Functional disorders of polymorphonuclear neutrophils
CHED.HIG;CHEDIAK-HIGASHI SYNDROME;E70.3;Albinism
CID;COMBINED IMMUNODEFICIENCY;D81;Combined immunodeficiencies
CMC;CHRONIC MUCOCUTANEOUS CANDIDIASIS;NONE;B37.2
CMCC;CMCC;NONE;NO_ICD
COH;CHRONIC AUTOIMMUNE HEPATITIS;K75.4;Autoimmune hepatitis
CVID;COMMON VARIABLE IMMUNODEFICIENCY;D83;Common variable immunodeficiency
HLA II DEF;HLA II DEFICIENCY;NONE;NO_ICD
COMP.DEF;COMPLEMAN DEFICIENCY;NONE;NO_ICD
F.MENINGO;FULMINAN MENINGOKOKSEMI;NONE;NO_ICD
GRISCELLI;GRISCELLI SYNDROME;NONE;NO_ICD
HYPER IGD;HYPER IGD SYNDROME;NONE;NO_ICD
H.GLOBULIN;HYPOGAMMAGLOBULINEMIA;D80.0;Hereditary hypogammaglobulinaemia
HSE;HSE;NONE;NO_ICD
HYDROEST.;HYDROESTIVALE;NONE;NO_ICD
HYPER IGE;HYPER IGE SYNDROME;D82.4;Hyperimmunoglobulin E [IgE] syndrome
HYPER IGM;HYPER IGM SYNDROME;D80.5;Immunodeficiency with increased immunoglobulin M [IgM]
IGM DEF;IGM DEFICIENCY;D80.4;Selective deficiency of immunoglobulin M [IgM]
IRAK 4;IRAK 4  DEFICIENCY;NONE;NO_ICD
IL-12;IL-12;NONE;NO_ICD
K12 DEF;K12 DEFICIENCY;NONE;NO_ICD
LAD;LEUKOCYTE ADHESION DEFICIENCY;NONE;D84.8
MHC DEF;MHC CLASS II DEFICIENCY;NONE;NO_ICD
MBL;MANNOSE BINDING LECTIN DEFICIENCY;NONE;NO_ICD
MDF;MDF;NONE;NO_ICD
MSMD;MSMD;NONE;NO_ICD
OMENN;OMENN SYNDROME;D81.2;Severe combined immunodeficiency [SCID] with low or normal B-cell numbers
OSTEOMYELIT;OSTEOMYELIT;NONE;NO_ICD
CN;CYCLIC NEUTROPENIA;NONE;NO_ICD
POLYMIYOSIT;P.MIYOSIT;NONE;NO_ICD
SCID;SEVERE COMBINED IMMUNODEFICIENCY;D81.0;Severe combined immunodeficiency [SCID] with reticular dysgenesis
T CELL DEF;T CELL DEFICIENCY;NONE;NO_ICD
TCR-CD3;TCR-CD3 EXPRESSION DEFECT;NONE;NO_ICD
THG;TRANSIENT HYPOGAMMAGLOBULINEMIA;NONE;NO_ICD
VARIANT AT;VARIANT A-T;NONE;NO_ICD
WAS;WISCOTT ALDRICH SYNDROME;NONE;D82.0
WHIM;WHIM;NONE;NO_ICD
XLA;X-LINKED AGAMMAGLOBULINEMIA;D80.0;Hereditary hypogammaglobulinaemia
ALL;ACUTE LYMPHOBLASTIC LEUKEMIA;C91.0;Acute lymphoblastic leukaemia [ALL]
AML;ACUTE MYELOBLASTIC LEUKEMIA;NONE;C92.0
CDA;CDA;NONE;D64.4
CHR.ITP;CHRONIC IDIOPATHIC TROMBOCYTOPENIA;NONE;NO_ICD
FAA;FANCONI APLASTIC ANEMIA;NONE;NO_ICD
GLAN.TROMB;GLANZMAN TROMBASTENIA;NONE;NO_ICD
HYPEREOSIN;HYPEREOSINOPHYLIA SYNDROME;NONE;NO_ICD
H.SPHERO.;HEREDITARY SPHEROCYTOSIS;D58.0;Hereditary spherocytosis
HEM A;HEMOPHILIA A;D66;Hereditary factor VIII deficiency
HEM B;HEMOPHILIA B;D67;Hereditary factor IX deficiency
HEM.ANEMIA;CHRONIC HEMOLYTIC ANEMIA;NONE;NO_ICD
HEMOPHAGO.;HEMOPHAGOCYTIC SYNDROME;D76.1;Haemophagocytic lymphohistiocytosis
HF;HYPOFIBRINOGENEMIA;NONE;P56.9
HYP.ANEMIA;HYPOPLASTIC ANEMIA;NONE;NO_ICD
KML;KML;NONE;NO_ICD
MDS;MYELODYSPLASTIC SYNDROME;D46;Myelodysplastic syndromes
MEG.ANEMIA;MEGALOBLASTIC ANEMIA;D51.1;Vitamin B12 deficiency anaemia due to selective vitamin B12 malabsorption with proteinuria
RAEB-T;REFRACTORY ANEMIA WITH EXCESS BLAST-T;NONE;D46.3
THALAS.;THALASSEMIA;D56;Thalassaemia
VWD;VON-WILLEBRAND’S DISEASE;NONE;NO_ICD
A.ANEMIA;APLASTIC ANEMIA;D60;Acquired pure red cell aplasia [erythroblastopenia]
FHL;FAMILIAL HEMOPHAGOCYSTIC LYMPHOHISTIOCYTOSIS;NONE;NO_ICD
OSTEOPET;OSTEOPETROSIS;Q78.2;Osteopetrosis
BRONCHIEK;BRONCHIEKTIASIS;NONE;NO_ICD
C.LUNG;CHRONIC LUNG DISEASE;NONE;NO_ICD
CF;CYSTIC FIBROSIS;E84;Cystic fibrosis
D.CONGENIT;DUSHENOTEUS CONGENITA;NONE;NO_ICD
KARTAGENER;KARTAGENER SYNDROME;Q89.3;Situs inversus
PNEUMONIA;PNEUMONIA;J12;Viral pneumonia, not elsewhere classified
PCD;PRIMARY CILIARY DYSKINESIA;Q89.3;Situs inversus
TC;TUBERCULOSIS;A15;Respiratory tuberculosis, bacteriologically and histologically confirmed
M.ABSENCES;EPILEPSY WITH MYOCLONIC ABSENCES;NONE;NO_ICD
ADEM;ACUTE DISSEMINATED ENCEPHALOMYELITIS;G04.0;Acute disseminated encephalitis
ALD;ADRENOLEUKODYSTROPHY;E71.3;Disorders of fatty-acid metabolism
ALEXANDER;ALEXANDER DISEASE;E75.2;Other sphingolipidosis
ALICARDI;ALICARDI SYNDROME;NONE;NO_ICD
ALLGROWE;ALLGROWE SYNDROME;NONE;NO_ICD
ALZHEIMER;ALZHEIMER DISEASE;G30;Alzheimer disease
ALPERT;ALPERT SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
ALS;ALS;G12.2;Motor neuron disease
AMC;ARTHROGRYPOSIS MULTIPLEX CONGENITA;Q74.3;Arthrogryposis multiplex congenita
ANALGESIA;ANALGESIA;NONE;NO_ICD
AMN;ADRENOMYELONEUROPATHY;E71.3;Disorders of fatty-acid metabolism
ANDERMANN;ANDERMANN SYNDROME;NONE;NO_ICD
ANEVRIZMA;ANEVRIZMA;NONE;NO_ICD
AS;ANDERSEN SYNDROME;I45.8;Other specified conduction disorders
ANGELMAN;ALGELMAN SYNDROME;NONE;NO_ICD
ISP;IDIOPATHIC SPASTIC PARAPARESY;NONE;NO_ICD
AXON.NEUR;AXONAL NEUROPATHY;NONE;NO_ICD
BFIC;BENIGN FAMILIAL INFANTILE CONVULSION;NONE;NO_ICD
BIM;BENIGN INFANTILE MYOCLONUS;NONE;NO_ICD
BMD;BECKER MUSCULAR DYSTROPHY;G71.0;Muscular dystrophy
BME;BENIGN MYOCLONIC EPILEPSY;NONE;NO_ICD
BPS;BILATERAL PERISYLVIAN SYNDROME (MCD);NONE;NO_ICD
BRE;BENIGN ROLANDIC EPILEPSY;G40.0;Localization-related (focal)(partial) idiopathic epilepsy and epileptic syndromes with seizures of localized onset
BSN;BILATERAL STRIATAL NECROSIS;NONE;NO_ICD
BTA;BILATERAL TEMPORAL ATROPHY;NONE;NO_ICD
CADASIL;CADASIL;NONE;I67.8
CASH SYNDROME;CASH;NONE;NO_ICD
C-SAGUENAY;CHARLEVOIX-SAGUENAY DISEASE;NONE;NO_ICD
CALPAIN;CALPAIN DEFICIENCY;NONE;NO_ICD
CANAVAN;CANAVAN DISEASE;E75.2;Other sphingolipidosis
CCD;CORPUS COLLOSUM DEFECT;NONE;NO_ICD
CDG;CARBOHYDRATE DEFICIENT GLUCOPROTEIN SYNDROME (CONGENITAL DEFECTS OF GLYCOSYLATION);NONE;NO_ICD
CAT;CEREBELLAR ATROPHY;NONE;NO_ICD
CER.HYPO.;CEREBELLAR HYPOPLASIA;NONE;NO_ICD
CVD;CEREBROVASCULAR DISORDER;I60;Subarachnoid haemorrhage
CHAR.MARIE;CHARCOTT MARIE TOOTH DISEASE;NONE;NO_ICD
CHER.SPOT;CHERRY RED SPOT;NONE;NO_ICD
CM;CONGENITAL MYASTHENIA;NONE;NO_ICD
CMD;CONGENITAL MUSCULAR DYSTROPHY;G71.2;Congenital myopathies
CNS;CENTRAL NERVOUS SYSTEM DISEASE;NONE;NO_ICD
COCKAYNE;COCKAYNE SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
CON.MYOPA;CONGENITAL MYOPATHY;G71.2;Congenital myopathies
CREATINE D;CREATINE DEFICIENCY;NONE;NO_ICD
C-JAKOB;CREUTZFELDT-JAKOB;A81.0;Creutzfeldt-Jakob disease
COE Q DEF;COENZYME Q  DEFICIENCY;NONE;NO_ICD
MCD;MALFORMATION OF CORTICAL DEVELOPMENT(KORTIKAL GELISIM ANOMALISI);NONE;E53.8
D.CORTEX;DOUBLE CORTEX;NONE;NO_ICD
DEJ-SOTTAS;DEJERINE-SOTTAS DISEASE;G60.0;Hereditary motor and sensory neuropathy
DEM.NEUR;DEMYELINATING PERIPHERIC NEUROPATHY;NONE;NO_ICD
DEMANS;DEMANS;NONE;NO_ICD
DM;MYOTONIC DYSTROPHY (DYSTROPHIA MYOTONICA);NONE;M33.1
DMD;DUCHENNE MUSCULAR DYSTROPHY;G71.0;Muscular dystrophy
DRAVET;DRAVET;G40.3;Generalized idiopathic epilepsy and epileptic syndromes
DRD;DOPA RESPONSIVE DYSTONIA;NONE;NO_ICD
DNET;DNET;NONE;NO_ICD
DYSTROFIN;DYSTROFINOPATHY;NONE;NO_ICD
DYSTONIA;DISTONI;G24.9;Dystonia, unspecified
EMD;EMERY-DREYFUS MUSCULAR DYSTROPHY;NONE;NO_ICD
EPILEPSY;EPILEPSY;G40;Epilepsy
EXPYR.;EXTRAPYRAMIDAL FINDINGS/FEATURES;NONE;NO_ICD
EYELID M.A;EYELID MYOCLONIA WITH ABSENCES;NONE;NO_ICD
F.SPOS;FAMILIAL SPOSMUTANS;NONE;NO_ICD
F.ABSENCES;FAMILIAL ABSENCES;NONE;NO_ICD
F.DEMENTIA;FRONTOTEMPORAL DEMENTIA;G31.0;Circumscribed brain atrophy
PP;PERIODIC PARALYSIS;G72.3;Periodic paralysis
FA;FRIEDREICH ATAXIA;G11.1;Early-onset cerebellar ataxia
FAHR;FAHR;G23.8;Other specified degenerative diseases of basal ganglia
FAM.CHOREO;FAMILIAL CHOREOATHETOSIS;NONE;NO_ICD
FKRB MUTATION;FKRB MUTATION;NONE;NO_ICD
F.PARESY;FACIAL PARESY;NONE;NO_ICD
FC;FEBRILE CONVULSION;R56.0;Febrile convulsions
FSHD;FACIO-SCAPULO HUMORAL MD SYNDROME;NONE;G71.0
GAN;GIANT AXONAL NEUROPATHY;G60.8;Other hereditary and idiopathic neuropathies
GANGLIOSID;GANGLIOSIDOSIS;E75.0;GM2 gangliosidosis
G-BARRE;GUILLAIN-BARRE SYNDROME;G61.0;Guillain-Barr� syndrome
GLUT.ACID;GLUTARIC ACIDURIA;NONE;NO_ICD
HYDROCEPH;HYDROCEPHALY;G91;Hydrocephalus
H-SPATZ;HALLERVORDEN-SPATZ DISEASE;G23.0;Hallervorden-Spatz disease
H.CHOREO.;HEREDITARY CHOREOATHETOSIS;NONE;NO_ICD
HA;HEREDITARY ATAXIA;NONE;NO_ICD
HD;HUNTINGTON DISEASE;G10;Huntington disease
HM;HEMIMEGALOENCEPHALY;NONE;NO_ICD
HEMIDYSTONIA;HEMIDISTONI;NONE;NO_ICD
HER.INSENS;HEREDITARY INSENSITIVITY TO PAIN;NONE;NO_ICD
HETEROTOP;HETEROTOPHY (MCD);NONE;NO_ICD
HYPOPLASIA;HYPOPLASIA (MCD);NONE;NO_ICD
HN;HEREDITARY NEUROPATHY;NONE;NO_ICD
HSAN;HEREDITARY SPASTIC AXONAL NEUROPATHY;NONE;NO_ICD
HSMN;HEREDITARY SENSORY-MOTOR NEUROPATHY;NONE;NO_ICD
HSP;HEREDITARY SPASTIC PARAPLEGIA;G11.4;Hereditary spastic paraplegia
IFFE;IDIOPATHIC FAMILIAL FOCAL EPILEPSY;NONE;NO_ICD
ITD;IDIOPATHIC TORSION DYSTONIA;G24.9;Dystonia, unspecified
INCONT PIG;INCONTINENTIA PIGMENT;NONE;NO_ICD
JME;JUVENILE MYOCLONIC EPILEPSY;G40.3;Generalized idiopathic epilepsy and epileptic syndromes
JOUBERT;JOUBERT SYNDROME;Q04.3;Other reduction deformities of brain
C.NONCOMP;CARDIAC NONCOMPACTION;NONE;NO_ICD
KENNEDY;KENNEDY SYNDROME;G12.1;Other inherited spinal muscular atrophy
KRABBE;KRABBE DISEASE;E75.2;Other sphingolipidosis
KWS;KUGELBERG-WELANDER SYNDROME;G12.0;Infantile spinal muscular atrophy, type I [Werdnig-Hoffman]
LAFORA;LAFORA DISEASE;G40.3;Generalized idiopathic epilepsy and epileptic syndromes
LBD;LEWY BODY DEMENTIA;G31.8;Other specified degenerative diseases of nervous system
LBSL;LBSL;NONE;E75.2
LCA;LEBERS CONGENITAL AMAUROSIS;NONE;NO_ICD
L.DISEASE;LISOSOMAL DISEASE;NONE;NO_ICD
LD;LEUKODYSTROPHY;E75.2;Other sphingolipidosis
LEOPARD;LEOPARD SYNDROME;NONE;NO_ICD
L-GASTAUT;LENNOX-GASTAUT SYNDROME;G40.4;Other generalized epilepsy and epileptic syndromes
LGMD;LIMB-GIRDLE MUSCULAR DYSTROPHY;G71.0;Muscular dystrophy
LHON;LHON;H47.2;Optic atrophy
LISSENCEPH;LISSENCEPHALY (MCD);NONE;NO_ICD
M.DYSTONIA;MYOTONIC DYSTONIA;NONE;NO_ICD
M.G;MYASTHENIA GRAVIS;G70.0;Myasthenia gravis
MD;MUSCULAR DYSTROPHY;G71.0;Muscular dystrophy
ME LD;MEGALOENCEPHALY LEUKODYSTROPHY;NONE;NO_ICD
MEB-D;MUSCLE-EYE-BRAIN DISEASE;NONE;NO_ICD
MENINGITIS;MENINGITIS;G00;Bacterial meningitis, not elsewhere classified
MET LD;METACHROMATIC LEUKODYSTROPHY;E75.2;Other sphingolipidosis
MIGREN;MIGREN;G43;Migraine
MIT;NARP,MITOCHONDRIAL CYTOPATHY,MERRF,CYTOCHROME OXIDASE C DEFICIENCY,LEIGH,CPEO+MYOPATHY,MELAS,MITOCHONDRIAL DELETION,DUBLICATION;NONE;NO_ICD
MICROCEPH;MICROCEPHALY;Q02;Microcephaly
MILLER-DIECKER SYNDROME;M.DIECKER;NONE;NO_ICD
MMR;MENTAL MOTOR RETARDATION;NONE;NO_ICD
MOEBIUS;MOEBIUS SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
MOLIBDEN GF;MOLIBDEN GF DEFICIENCY;NONE;NO_ICD
MN.DISEASE;MOTOR NEURON DISEASE;G12.2;Motor neuron disease
MR;MENTAL RETARDATION;F70;Mild mental retardation
MS;MULTIPLE SCLEROSIS;G35;Multiple sclerosis
MTS;MESIAL TEMPORAL SCLEROSIS;NONE;NO_ICD
MULTICORE;MULTICORE DISEASE;NONE;NO_ICD
MYASTHENIA;MYASTHENIA;NONE;NO_ICD
MYELINOPATHY;MYELIN;G60;Hereditary and idiopathic neuropathy
MYOPATHY;MYOPATHY;G71;Primary disorders of muscles
MYOT.MYOPA;MYOTUBULAR MYOPATHY;G71.2;Congenital myopathies
N.ACANTHO.;NEUROACANTHOCYTOSIS;NONE;NO_ICD
NAD;NEUROAXONAL DYSTROPHY;NONE;NO_ICD
NARCOLEPSY;NARCOLEPSY;G47.4;Narcolepsy and cataplexy
N.BEHCET;NEUROBEHCET;NONE;NO_ICD
NEUROPATHY;NEUROPATHY;G64;Other disorders of peripheral nervous system
MEUROMYOPATHY;NEUROMYOPATHY;NONE;NO_ICD
NCL;NEURONAL CEROID LIPOFUSCINOSIS;E75.4;Neuronal ceroid lipofuscinosis
NDM;NONDYSTROPHIC MYOTONIA;NONE;P70.2
NF;NEUROFIBROMATOSIS;Q85.0;Neurofibromatosis (nonmalignant)
ND;NEUROTRANSMITTER DEFECTS;NONE;NO_ICD
NM;NEMALINE CEROID MYOPATHY;NONE;G71.2
NM.DISEASE;NEUROMETABOLIC DISEASE;NONE;NO_ICD
O.NEURITIS;OPTIC NEURITIS;H46;Optic neuritis
OCD;OBSESSIVE-COMPULSIVE DISORDER DIAGNOSIS;NONE;NO_ICD
OLE;OCCIPITAL LOBE EPILEPSY;NONE;NO_ICD
ORG.ACID;ORGANIC ACIDEMIA;NONE;NO_ICD
OTISIM;OTISIM;NONE;NO_ICD
OVERLOP;OVERLOP SYNDROME;NONE;NO_ICD
PYRIDOXINE;PYRIDOXINE DEFICIENCY;E53.1;Pyridoxine deficiency
P.CONGENIT;PARAMYOTONIA CONGENITA;G71.1;Myotonic disorders
PD;PARKINSON DISEASE;G20;Parkinson disease
PERRY;PERRY SYNDROME;NONE;NO_ICD
PETT LIKE;PETT LIKE SYNDROME;NONE;NO_ICD
PLM;PELIZAEUS-MERZBACHER DISEASE;E75.2;Other sphingolipidosis
PMD;PROGRESSIVE MUSCULAR DYSTROPHY;NONE;E75.2
PME;PROGRESSIVE MYOCLONUS EPILEPSY;NONE;NO_ICD
POLYMICROG;POLYMICROGYRIA (MCD);NONE;NO_ICD
POMPE;POMPE;E74.0;Glycogen storage disease
RASMUSSEN;RASMUSSEN;NONE;NO_ICD
REFSUM;REFSUM DISEASE;G60.1;Refsum disease
RPC;RELAPSING POLYCHONDRISIT;NONE;NO_ICD
RETT;RETT SYNDROME;F84.2;Rett syndrome
RILEY DAY;RILEY- DAY SYNDROME;NONE;NO_ICD
CA;CEREBELLAR ATAXIA;G11.1;Early-onset cerebellar ataxia
RIGIT SPINE SYNDROME;RSS;NONE;NO_ICD
SACRAL AGE;SACRAL AGENESIA;NONE;NO_ICD
SANDHOFF;SANDHOFF DISEASE;E75.0;GM2 gangliosidosis
SCA;SPINOCEREBELLAR ATAXIA;NONE;NO_ICD
S-JAMPEL;SCHWARTZ-JAMPEL SYNDROME;NONE;NO_ICD
SCOLIOSIS;SCOLIOSIS;M41;Scoliosis
SEGAWA;SEGAWA DISEASE;NONE;NO_ICD
SELENO;SELONA;NONE;NO_ICD
SCHIZOPHRENIA;SCHIZOPHRENIA;F20;Schizophrenia
SHC;SEMILOBAR HOLOPROSENCEPHALY;NONE;NO_ICD
SMA;SPINAL MUSCULAR ATROPHY;G12.0;Infantile spinal muscular atrophy, type I [Werdnig-Hoffman]
SMARD;SMARD;NONE;G12.2
SP.CER.DEG;SPINOCEREBELLAR DEGENERATION;NONE;NO_ICD
SPE;SYMPTOMATIC PARTIAL EPILEPSY;NONE;NO_ICD
SVO;SVO;NONE;NO_ICD
SED;SPONDYLOEPIPHSEAL DYSPLASIA;NONE;NO_ICD
T.HYDROXY;TYROSIN HYDROXYLASE DEFICIENCY;NONE;NO_ICD
TANGIER;TANGIER DISEASE;E78.6;Lipoprotein deficiency
TAY SACHS;TAY SACHS DISEASE;E75.0;GM2 gangliosidosis
TLE;TEMPORAL LOBE EPILEPSY;G40.1;Localization-related (focal)(partial) symptomatic epilepsy and epileptic syndromes with simple partial seizures
TOURETTE;TOURETTE SYNDROME;F95.2;Combined vocal and multiple motor tic disorder [de la Tourette]
ULD;UNVERRICHT-LUNDBORG DISEASE;G40.3;Generalized idiopathic epilepsy and epileptic syndromes
ULLRICH;ULLRICH SYNDROME;NONE;NO_ICD
VAN LD;VANISHING LEUKODYSTROPHY;NONE;NO_ICD
VWM;VANISHING WHITE MATTER;NONE;NO_ICD
WWS;WALKER-WARBURG SYNDROME;NONE;Q04.3
ND;NEURODEGENERATIVE DISORDER;G30;Alzheimer disease
FAHR;FAHR DISEASE;G23.8;Other specified degenerative diseases of basal ganglia
FR.X;FRAGILE-X SYNDROME;Q99.2;Fragile X chromosome
KMG;KMG;NONE;NO_ICD
KOK;KOK DISEASE;NONE;NO_ICD
P.TUMOR;PONTOCEREBELLAR TUMOR;NONE;NO_ICD
NCS;NEUROCUTENOUS SYNDROME;NONE;NO_ICD
PHM;PAROXYSMAL HYPNOGENIC MYOCLONIA;NONE;NO_ICD
PKC;PAROXYSMAL KINESIGENIC CHOREOATHETOSIS;NONE;NO_ICD
POST.APH.;POSTTRAVMATIC APHASIA;NONE;NO_ICD
RSS;RIGID SPINE SYNDROME;NONE;NO_ICD
S-WAARDEN.;SHARL-WAARDENBURG SYNDROME;NONE;NO_ICD
C.HYPOPLAS;CEREBELLAR HYPOPLASIA;NONE;NO_ICD
SE;SCHIZENCEPHALY (MCD);NONE;NO_ICD
SLS;SJOGREN-LARSSON SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
MCNS;MALFORMATION OF THE CENTRAL NERVOUS SYSTEM;NONE;NO_ICD
SSPE;SUBACUTE SCLEROSING PANENCEPHALITIS;A81.1;Subacute sclerosing panencephalitis
UDL;UNDIAGNOSED LEUKODYSTROPHY;NONE;NO_ICD
V.D.KNAAP;VAN DER KNAAP DISEASE;NONE;NO_ICD
VLC;VANISHING LEUKOENCEPHALOPATHY;NONE;NO_ICD
WOOLY-HAIR;WOOLY-HAIR DISEASE;NONE;NO_ICD
CTD;CENTROTUBULAR DYSTROPHY;NONE;NO_ICD
SLO;SMITH LEMLI OPITZ SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
C.PALSY;CEREBRAL PALSY;G80;Cerebral palsy
DYSLEXIA;DYSLEXIA;R48.0;Dyslexia and alexia
E-LAMBERT;EATON-LAMBERT SYNDROME;G73.1;Lambert-Eaton syndrome
H.COPROP;HEREDITARY COPROPORPHYRIA;E80.2;Other porphyria
WH;WERDNIG-HOFMANN DISEASE;NONE;NO_ICD
BIPOLAR;BIPOLAR;F31;Bipolar affective disorder
PMA;PERONEAL MUSCULAR ATROPHY;G60.0;Hereditary motor and sensory neuropathy
CCD;CLEIDO CRANIAL DYSPLASIA;NONE;NO_ICD
CAT;CEREBELLAR ATROPHY;NONE;NO_ICD
NB;NEUROBLASTOMA;C74.9;Malignant neoplasm: Adrenal gland, unspecified
MENKES;MENKES SYNDROME;E83.0;Disorders of copper metabolism
WEST;WEST SYNDROME;G40.4;Other generalized epilepsy and epileptic syndromes
WILMS;WILMS TUMORU;NONE;NO_ICD
ASD;ATRIAL SEPTAL DEFECT;Q21.1;Atrial septal defect
CHD;CONGENITAL HEART DEFECT;Q20;Congenital malformations of cardiac chambers and connections
CMP;CARDIOMYOPATHY;I42.0;Dilated cardiomyopathy
DCMP;DILATE CARDIOMYOPATHY;NONE;NO_ICD
HCMP;HIPER CARDIOMYOPATHY;NONE;NO_ICD
HVT;HIPERODRENERJIK VT;NONE;NO_ICD
TGA;TRANSPOSITION OF GREAT ARTERIES;NONE;NO_ICD
RCMP;RESTRICTIVE CARDIOMYOPATHY;I42.5;Other restrictive cardiomyopathy
CDCM;CONGENITAL DILATED CARDIOMYOPATHY;NONE;NO_ICD
POLYMORPHIC VT;PVT;I47.2;Ventricular tachycardia
QTS;LONG QT SYNDROME;I45.8;Other specified conduction disorders
DUANE;DUANE TIP 3;NONE;NO_ICD
F.CATARACT;FAMILIAL CATARACT;NONE;NO_ICD
FABRY;FABRY;E75.2;Other sphingolipidosis
GLAUCOMA;GLAUCOMA;H40;Glaucoma
KNIEST;KNIEST SYNDROME;Q77.8;Other osteochondrodysplasia with defects of growth of tubular bones and spine
RB;RETINOBLASTOMA;C69.2;Malignant neoplasm: Retina
RP;RETINITIS RIGMENTOSA;NONE;NO_ICD
RETINOPAT.;RETINOPATHY;H35;Other retinal disorders
OPHT.ETIO;OPTHALMOPARESIA ETIOLOGY;NONE;NO_ICD
M.GOLD;MACULAR GOLD;NONE;NO_ICD
M.KOLOBOM;MACULAR KOLOBOM;NONE;NO_ICD
USCHER;USCHER  SYNDROME;NONE;NO_ICD
GORLIN;GORLIN SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
PCOS;PCOS;E28.2;Polycystic ovarian syndrome
AN;ANOREXIA NERVOZA;NONE;NO_ICD
ATRICHIA;ATRICHIA;NONE;NO_ICD
ACNE;ACNE;L70.0;Acne vulgaris
DC;DYSKERATOSIS CONGENITA;Q82.8;Other specified congenital malformations of skin
ERITRODERMIA;ERITRODERMIA;NONE;NO_ICD
ICHTHYOSIS;ICHTHYOSIS;Q80;Congenital ichthyosis
KINDLER;KINDLER DISEASE;NONE;NO_ICD
MONILETRIX;MONILETRIX;NONE;NO_ICD
MEN TYPE 2B;MEN TYPE 2B;NONE;NO_ICD
LIPOID PROTEINOSIS;LP;E78.8;Other disorders of lipoprotein metabolism
PAPILLA LEFEVRE;P.LEFEVRE;NONE;NO_ICD
WERNER;WERNER SYNDROME;NONE;NO_ICD
FALLOT;FALLOT TETRATOLOGY;NONE;NO_ICD
EB;EPIDERMOLYSIS BULLOSA;Q81;Epidermolysis bullosa
DIGEORGE;DIGEORGE SYNDROME;D82.1;Di George syndrome
E.DYSPLASI;ECTODERMAL DYSPLASIA;Q82.4;Ectodermal dysplasia (anhidrotic)
HC;HEMOCHROMATOSIS;R79.0;Abnormal level of blood mineral
VGD;VON GIERKE DISEASE;E74.0;Glycogen storage disease
MASTOCYTOS;MASTOCYTOSIS;Q82.2;Mastocytosis
MELANOMA;MALIGNANT MELANOMA;C43;Malignant melanoma of skin
NEOPLAZM;MALIGN NEOPLAZM;NONE;NO_ICD
PREMATURIT;PREMATURITY;NONE;NO_ICD
HK;HEREDITARY KERATODERMA;NONE;NO_ICD
HUS;HEMOLYTIC UREMIC SYNDROME;D59.3;Haemolytic-uraemic syndrome
IHE;INFANTILE HEMANGIOENDOTHELIOMA;NONE;NO_ICD
TINER;TINER INTOCSICATION;NONE;NO_ICD
PE;POSTPARTUM EDITUS;NONE;NO_ICD
LFD;LI-FREUMENI DISEASE;NONE;NO_ICD
WGD;WEGENER GRANULAR DISEASE;NONE;NO_ICD
ICHTHYOSIS;ICHTHYOSIS;Q80;Congenital ichthyosis
XP;XERODERMA PIGMENTOSUM;Q82.1;Xeroderma pigmentosum
S-LARSON;SJOGREN-LARSON DISEASE;NONE;NO_ICD
CCSV;CCSV;NONE;NO_ICD
