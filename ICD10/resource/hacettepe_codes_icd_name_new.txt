ACRODERMA;ACRODERMATITIS ENTEROPATHICA;E83.2;Disorders of zinc metabolism
ALKAPTON;ALKAPTONURIA;E70.2;Disorders of tyrosine metabolism
ARGININEMIA;ARGININEMIA;NONE;E72.2
ASA;ARGININOSUCCINIC ACIDURIA;E72.2;Disorders of urea cycle metabolism
BIOT.DEF;BIOTINIDASE DEFICIENCY;E53.8;Deficiency of other specified B group vitamins
CYSTINOSIS;CYSTINOSIS;E72.0;Disorders of amino-acid transport
CYSTINURIA;CYSTINURIA;E72.0;Disorders of amino-acid transport
CROUZON;CROUZON SYNDROME;Q75.1;Craniofacial dysostosis
F1,6D DEF;FRUCTOSE-1,6-DIPHOSPHATASE DEFICIENCY;E74.1;Disorders of fructose metabolism
FARBER;FARBER DISEASE;E75.2;Other sphingolipidosis
GALACTOSEM;GALACTOSEMIA;E74.2;Disorders of galactose metabolism
GOLDENHAR;GOLDENHAR SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
HFI;HEREDITARY FRUCTOSE INTOLERANCE;E74.1;Disorders of fructose metabolism
HYPERCHOL.;HYPERCHOLESTEROLEMIA;E78.0;Pure hypercholesterolaemia
H.AMMONEM.;HYPERAMMONEMIA;E72.2;Disorders of urea cycle metabolism
HYPERLIPID;HYPERLIPIDEMIA;E78;Disorders of lipoprotein metabolism and other lipidaemias
HYPERLYSIN;HYPERLYSINEMIA;E72.3;Disorders of lysine and hydroxylysine metabolism
FHCM;FAMILIAL HYPERCHYLOMICRONEMIA;E78;Disorders of lipoprotein metabolism and other lipidaemias
HYPOMAGNES;HYPOMAGNESEMIA;E83.4;Disorders of magnesium metabolism
HOMOCYS.;HOMOCYSTINURIA;E72.1;Disorders of sulfur-bearing amino-acid metabolism
HTG;HYPERTRIGLYCERIDEMIA;E78.1;Pure hyperglyceridaemia
HURLER;HURLER SYNDROME;E76.0;Mucopolysaccharidosis, type I
IVA;ISOVALERIC ACIDEMIA;E71.1;Other disorders of branched-chain amino-acid metabolism
CARNITINE;CARNITINE DEFICIENCY;E71.3;Disorders of fatty-acid metabolism
L.NYHAN;LESCH-NYHAN SYNDROME;E79.1;Lesch-Nyhan syndrome
LCAD;LCAD DEFICIENCY;NONE;E71.3
LPI;LYSINURIC PROTEIN INTOLERANCE;E72.3;Disorders of lysine and hydroxylysine metabolism
LPL DEF;LIPOPROTEIN LIPASE DEFICIENCY;E78;Disorders of lipoprotein metabolism and other lipidaemias
MUCOLIPID;MUCOLIPIDOSIS;E77.0;Defects in post-translational modification of lysosomal enzymes
MMA;METHYLMALONIC ACIDEMIA;E71.1;Other disorders of branched-chain amino-acid metabolism
MSUD;MAPLE SYRUP URINE DISEASE;E71.0;Maple-syrup-urine disease
OBESITY;OBESITY;E66;Obesity
OTC;ORNITHINE TRANSCARBAMYLASE DEFICIENCY;E72.4;Disorders of ornithine metabolism
PEROXISOM;PEROXISOMAL DISEASE;E80.3;Defects of catalase and peroxidase
PKU;PHENYLKETONURIA;E70.0;Classical phenylketonuria
PROTEINURI;PROTEINURIA;R80;Isolated proteinuria
PHA;PSEUDOHYPOALDOSTERONISM;N25.8;Other disorders resulting from impaired renal tubular function
P.C.DEF;PYRUVATE CARBOXYLASE DEFICIENCY;E74.4;Disorders of pyruvate metabolism and gluconeogenesis
PROP.ACID;PROPIONIC ACIDEMIA;E71.1;Other disorders of branched-chain amino-acid metabolism
CITRULLIN.;CITRULLINEMIA;E72.2;Disorders of urea cycle metabolism
SOD;SULPHITE OXIDASE DEFICIENCY;NONE;Q04.8
TYROSIN.;TYROSINEMIA;E70.2;Disorders of tyrosine metabolism
UCD;UREA CYCLE DEFECT;E72.2;Disorders of urea cycle metabolism
VIT E DEF;VITAMIN E DEFICIENCY;E56.0;Deficiency of vitamin E
MOCOD;MOLYBDENUM COFACTOR DEFICIENCY;NONE;E72.1
ZELLWEGER;ZELLWEGER SYNDROME;Q87.8;Other specified congenital malformation syndromes, not elsewhere classified
ALAGILLE;ALAGILLE SYNDROME;Q44.7;Other congenital malformations of liver
CHOLESTERO;CHOLESTERYL ESTER STORAGE DISEASE;E75.5;Other lipid storage disorders
C.PANCREAT;CHRONIC PANCREATITIS;K86.0;Alcohol-induced chronic pancreatitis
CRIG.NAJ.;CRIGLER-NAJJAR SYNDROME;E80.5;Crigler-Najjar syndrome
G-G M;GLUCOSE-GALACTOSE MALABSORPTION;E74.3;Other disorders of intestinal carbohydrate absorption
GALACTOSIAL;GALACTOSIALIDOSIS;E77.1;Defects in glycoprotein degradation
GAUCHER;GAUCHER’S DISEASE;E75.2;Other sphingolipidosis
LSD;LIPID STORAGE DISEASE;E75;Disorders of sphingolipid metabolism and other lipid storage disorders
NIEM.PICK;NIEMANN PICK DISEASE;E75.2;Other sphingolipidosis
MUCOLIPID.;MUCOLIPIDOSIS;E77.0;Defects in post-translational modification of lysosomal enzymes
O-W-R;OSLER-WEBER-RENDU SYNDROME;I78.0;Hereditary haemorrhagic telangiectasia
PFIC;PROGRESSIVE FAMILIAL INTRAHEPATIC CHOLESTASIS;NONE;K83.1
WILSON;WILSON’S DISEASE;E83.0;Disorders of copper metabolism
AMYLOID;AMYLOIDOSIS;E85;Amyloidosis
BARTTER;BARTTER SYNDROME;E26.8;Other hyperaldosteronism
BEHCET;BEHCET’S DISEASE;M35.2;Beh�et disease
CNS;CONGENITAL NEPHROTIC SYNDROME;N04;Nephrotic syndrome
DRTA;DISTAL RENAL TUBULAR ACIDOSIS;NONE;N25.8
FSGS;FSGS;N00;Acute nephritic syndrome
FMF;FAMILIAL MEDITERRANEAN FEVER;E85.0;Non-neuropathic heredofamilial amyloidosis
GITELMAN;GITELMAN SYNDROME;N25.8;Other disorders resulting from impaired renal tubular function
IGA NEPH.;IGA NEPHROPATHY;N02.8;Recurrent and persistent haematuria: Other
JRA;JUVENILE RHEUMATOID ARTHRITIS;M08.0;Juvenile rheumatoid arthritis
LOWE;LOWE SYNDROME;E72.0;Disorders of amino-acid transport
NS;NEPHROTIC SYNDROME;N04;Nephrotic syndrome
PAN;POLYARTERITIS NODOSA;M30.0;Polyarteritis nodosa
PKD;POLYCYSTIC KIDNEY DISEASE;Q61;Cystic kidney disease
RTA;RENAL TUBULAR ACIDOSIS;N25.8;Other disorders resulting from impaired renal tubular function
SCLERODERMA;SCLERODERMA;L94.0;Localized scleroderma [morphea]
SLE;SYSTEMIC LUPUS ERYTHEMATOSUS;M32;Systemic lupus erythematosus
TS;TUBEROUS SCLEROSIS;Q85.1;Tuberous sclerosis
ADREN.S;ADRENOGENITAL SYNDROME;E25.0;Congenital adrenogenital disorders associated with enzyme deficiency
CAH;CONGENITAL ADRENAL HYPERPLASIA;E25.0;Congenital adrenogenital disorders associated with enzyme deficiency
DIABET.INS;DIABETES INSIPIDUS;E23.2;Diabetes insipidus
FHH;FAMILIAL HYPOCALSIURIC HYPERCALSEMIA;NONE;E83.5
GRAVES;GRAVES DISEASE;E05.0;Thyrotoxicosis with diffuse goitre
HYPERINS;HYPERINSULINISM;E15;Nondiabetic hypoglycaemic coma
HYPOCHOND.;HYPOCHONDROPLASIA;Q77.4;Achondroplasia
HYPOTHYRO.;HYPOTHYROIDISM;E03.9;Hypothyroidism, unspecified
LERIWEILL;LERI WEILL SYNDROME;Q77.8;Other osteochondrodysplasia with defects of growth of tubular bones and spine
PS.HPT;PSEUDOHYPOPARATHYROIDISM;E20.1;Pseudohypoparathyroidism
RICKETS;RICKETS;E55;Vitamin D deficiency
TEST.FEM;TESTICULAR FEMINIZATION;E34.5;Androgen resistance syndrome
THR;THYROID HORMONE  RESISTANCE;E07;Other disorders of thyroid
TYPE 1 DM;TYPE 1 DIABETES MELLITUS;E10;Insulin-dependent diabetes mellitus
APERT;APERT SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
BLOOM;BLOOM SYNDROME;Q82.8;Other specified congenital malformations of skin
C-H SYND.;CONRADI-HUNERMANN SYNDROME;Q77.3;Chondrodysplasia punctata
CARPENTER;CARPENTER SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
COIF;CONGENITAL ONYCHODYSPLASIA OF INDEX FINGER;NONE;Q84.6
D.INSIPIDUS;DIABETES INSIPIDUS;E23.2;Diabetes insipidus
GOLDENHAR;GOLDENHAR SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
I.VULGARIS;ICHTHYOSIS VULGARIS;Q80.0;Ichthyosis vulgaris
DOWN;DOWN SYNDROME;Q90;Down syndrome
L-M-B;LAURENCE-MOON-BIEDL SYNDROME;Q87.8;Other specified congenital malformation syndromes, not elsewhere classified
M.GRUBER;MECKEL-GRUBER SYNDROME;Q61.9;Cystic kidney disease, unspecified
N.PATELLA;NAIL PATELLA SYNDROME;Q87.2;Congenital malformation syndromes predominantly involving limbs
POLAND;POLAND SYNDROME;Q79.8;Other congenital malformations of musculoskeletal system
ROBINOW;ROBINOW SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
S.I.T.;SITUS INVERSUS TOTALIS;Q89.3;Situs inversus
SECKEL;SECKEL SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
T.COLLINS;TREACHER COLLINS SYNDROME;Q75.4;Mandibulofacial dysostosis
TD;THANATOPHORIC DYSPLASIA;Q77.1;Thanatophoric short stature
VACTER;VACTER SYNDROME;Q87.2;Congenital malformation syndromes predominantly involving limbs
VHLS;VON HIPPEL-LINDAU SYNDROME;Q85.8;Other phakomatoses, not elsewhere classified
WILLIAMS;WILLIAMS SYNDROME;Q93.8;Other deletions from the autosomes
COFS;CEREBRO-OCULO-FACIO-SKELETAL SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
WPW;WPW SYNDROME;I45.6;Pre-excitation syndrome
ADA DEF;ADENOSINE DEAMINASE DEFICIENCY;D81.3;Adenosine deaminase [ADA] deficiency
ALPS;AUTOIMMUNE LYMPHOPROLIFERATIVE SYNDROME;NONE;D72.8
B CELL DEFICIENCY;B CELL DEF;D80;Immunodeficiency with predominantly antibody defects
CGD;CHRONIC GRANULOMATOUS DISEASE;D71;Functional disorders of polymorphonuclear neutrophils
CHED.HIG;CHEDIAK-HIGASHI SYNDROME;E70.3;Albinism
CID;COMBINED IMMUNODEFICIENCY;D81;Combined immunodeficiencies
CMC;CHRONIC MUCOCUTANEOUS CANDIDIASIS;NONE;B37.2
COH;CHRONIC AUTOIMMUNE HEPATITIS;K75.4;Autoimmune hepatitis
CVID;COMMON VARIABLE IMMUNODEFICIENCY;D83;Common variable immunodeficiency
H.GLOBULIN;HYPOGAMMAGLOBULINEMIA;D80.0;Hereditary hypogammaglobulinaemia
HYPER IGE;HYPER IGE SYNDROME;D82.4;Hyperimmunoglobulin E [IgE] syndrome
HYPER IGM;HYPER IGM SYNDROME;D80.5;Immunodeficiency with increased immunoglobulin M [IgM]
IGM DEF;IGM DEFICIENCY;D80.4;Selective deficiency of immunoglobulin M [IgM]
LAD;LEUKOCYTE ADHESION DEFICIENCY;NONE;D84.8
OMENN;OMENN SYNDROME;D81.2;Severe combined immunodeficiency [SCID] with low or normal B-cell numbers
SCID;SEVERE COMBINED IMMUNODEFICIENCY;D81.0;Severe combined immunodeficiency [SCID] with reticular dysgenesis
WAS;WISCOTT ALDRICH SYNDROME;NONE;D82.0
XLA;X-LINKED AGAMMAGLOBULINEMIA;D80.0;Hereditary hypogammaglobulinaemia
ALL;ACUTE LYMPHOBLASTIC LEUKEMIA;C91.0;Acute lymphoblastic leukaemia [ALL]
AML;ACUTE MYELOBLASTIC LEUKEMIA;NONE;C92.0
CDA;CDA;NONE;D64.4
H.SPHERO.;HEREDITARY SPHEROCYTOSIS;D58.0;Hereditary spherocytosis
HEM A;HEMOPHILIA A;D66;Hereditary factor VIII deficiency
HEM B;HEMOPHILIA B;D67;Hereditary factor IX deficiency
HEMOPHAGO.;HEMOPHAGOCYTIC SYNDROME;D76.1;Haemophagocytic lymphohistiocytosis
HF;HYPOFIBRINOGENEMIA;NONE;P56.9
MDS;MYELODYSPLASTIC SYNDROME;D46;Myelodysplastic syndromes
MEG.ANEMIA;MEGALOBLASTIC ANEMIA;D51.1;Vitamin B12 deficiency anaemia due to selective vitamin B12 malabsorption with proteinuria
RAEB-T;REFRACTORY ANEMIA WITH EXCESS BLAST-T;NONE;D46.3
THALAS.;THALASSEMIA;D56;Thalassaemia
A.ANEMIA;APLASTIC ANEMIA;D60;Acquired pure red cell aplasia [erythroblastopenia]
OSTEOPET;OSTEOPETROSIS;Q78.2;Osteopetrosis
CF;CYSTIC FIBROSIS;E84;Cystic fibrosis
KARTAGENER;KARTAGENER SYNDROME;Q89.3;Situs inversus
PNEUMONIA;PNEUMONIA;J12;Viral pneumonia, not elsewhere classified
PCD;PRIMARY CILIARY DYSKINESIA;Q89.3;Situs inversus
TC;TUBERCULOSIS;A15;Respiratory tuberculosis, bacteriologically and histologically confirmed
ADEM;ACUTE DISSEMINATED ENCEPHALOMYELITIS;G04.0;Acute disseminated encephalitis
ALD;ADRENOLEUKODYSTROPHY;E71.3;Disorders of fatty-acid metabolism
ALEXANDER;ALEXANDER DISEASE;E75.2;Other sphingolipidosis
ALZHEIMER;ALZHEIMER DISEASE;G30;Alzheimer disease
ALPERT;ALPERT SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
ALS;ALS;G12.2;Motor neuron disease
AMC;ARTHROGRYPOSIS MULTIPLEX CONGENITA;Q74.3;Arthrogryposis multiplex congenita
AMN;ADRENOMYELONEUROPATHY;E71.3;Disorders of fatty-acid metabolism
AS;ANDERSEN SYNDROME;I45.8;Other specified conduction disorders
BMD;BECKER MUSCULAR DYSTROPHY;G71.0;Muscular dystrophy
BRE;BENIGN ROLANDIC EPILEPSY;G40.0;Localization-related (focal)(partial) idiopathic epilepsy and epileptic syndromes with seizures of localized onset
CADASIL;CADASIL;NONE;I67.8
CANAVAN;CANAVAN DISEASE;E75.2;Other sphingolipidosis
CVD;CEREBROVASCULAR DISORDER;I60;Subarachnoid haemorrhage
CMD;CONGENITAL MUSCULAR DYSTROPHY;G71.2;Congenital myopathies
COCKAYNE;COCKAYNE SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
CON.MYOPA;CONGENITAL MYOPATHY;G71.2;Congenital myopathies
C-JAKOB;CREUTZFELDT-JAKOB;A81.0;Creutzfeldt-Jakob disease
MCD;MALFORMATION OF CORTICAL DEVELOPMENT(KORTIKAL GELISIM ANOMALISI);NONE;E53.8
DEJ-SOTTAS;DEJERINE-SOTTAS DISEASE;G60.0;Hereditary motor and sensory neuropathy
DM;MYOTONIC DYSTROPHY (DYSTROPHIA MYOTONICA);NONE;M33.1
DMD;DUCHENNE MUSCULAR DYSTROPHY;G71.0;Muscular dystrophy
DRAVET;DRAVET;G40.3;Generalized idiopathic epilepsy and epileptic syndromes
DYSTONIA;DISTONI;G24.9;Dystonia, unspecified
EPILEPSY;EPILEPSY;G40;Epilepsy
F.DEMENTIA;FRONTOTEMPORAL DEMENTIA;G31.0;Circumscribed brain atrophy
PP;PERIODIC PARALYSIS;G72.3;Periodic paralysis
FA;FRIEDREICH ATAXIA;G11.1;Early-onset cerebellar ataxia
FAHR;FAHR;G23.8;Other specified degenerative diseases of basal ganglia
FC;FEBRILE CONVULSION;R56.0;Febrile convulsions
FSHD;FACIO-SCAPULO HUMORAL MD SYNDROME;NONE;G71.0
GAN;GIANT AXONAL NEUROPATHY;G60.8;Other hereditary and idiopathic neuropathies
GANGLIOSID;GANGLIOSIDOSIS;E75.0;GM2 gangliosidosis
G-BARRE;GUILLAIN-BARRE SYNDROME;G61.0;Guillain-Barr� syndrome
HYDROCEPH;HYDROCEPHALY;G91;Hydrocephalus
H-SPATZ;HALLERVORDEN-SPATZ DISEASE;G23.0;Hallervorden-Spatz disease
HD;HUNTINGTON DISEASE;G10;Huntington disease
HSP;HEREDITARY SPASTIC PARAPLEGIA;G11.4;Hereditary spastic paraplegia
ITD;IDIOPATHIC TORSION DYSTONIA;G24.9;Dystonia, unspecified
JME;JUVENILE MYOCLONIC EPILEPSY;G40.3;Generalized idiopathic epilepsy and epileptic syndromes
JOUBERT;JOUBERT SYNDROME;Q04.3;Other reduction deformities of brain
KENNEDY;KENNEDY SYNDROME;G12.1;Other inherited spinal muscular atrophy
KRABBE;KRABBE DISEASE;E75.2;Other sphingolipidosis
KWS;KUGELBERG-WELANDER SYNDROME;G12.0;Infantile spinal muscular atrophy, type I [Werdnig-Hoffman]
LAFORA;LAFORA DISEASE;G40.3;Generalized idiopathic epilepsy and epileptic syndromes
LBD;LEWY BODY DEMENTIA;G31.8;Other specified degenerative diseases of nervous system
LBSL;LBSL;NONE;E75.2
LD;LEUKODYSTROPHY;E75.2;Other sphingolipidosis
L-GASTAUT;LENNOX-GASTAUT SYNDROME;G40.4;Other generalized epilepsy and epileptic syndromes
LGMD;LIMB-GIRDLE MUSCULAR DYSTROPHY;G71.0;Muscular dystrophy
LHON;LHON;H47.2;Optic atrophy
M.G;MYASTHENIA GRAVIS;G70.0;Myasthenia gravis
MD;MUSCULAR DYSTROPHY;G71.0;Muscular dystrophy
MENINGITIS;MENINGITIS;G00;Bacterial meningitis, not elsewhere classified
MET LD;METACHROMATIC LEUKODYSTROPHY;E75.2;Other sphingolipidosis
MIGREN;MIGREN;G43;Migraine
MICROCEPH;MICROCEPHALY;Q02;Microcephaly
MOEBIUS;MOEBIUS SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
MN.DISEASE;MOTOR NEURON DISEASE;G12.2;Motor neuron disease
MR;MENTAL RETARDATION;F70;Mild mental retardation
MS;MULTIPLE SCLEROSIS;G35;Multiple sclerosis
MYELINOPATHY;MYELIN;G60;Hereditary and idiopathic neuropathy
MYOPATHY;MYOPATHY;G71;Primary disorders of muscles
MYOT.MYOPA;MYOTUBULAR MYOPATHY;G71.2;Congenital myopathies
NARCOLEPSY;NARCOLEPSY;G47.4;Narcolepsy and cataplexy
NEUROPATHY;NEUROPATHY;G64;Other disorders of peripheral nervous system
NCL;NEURONAL CEROID LIPOFUSCINOSIS;E75.4;Neuronal ceroid lipofuscinosis
NDM;NONDYSTROPHIC MYOTONIA;NONE;P70.2
NF;NEUROFIBROMATOSIS;Q85.0;Neurofibromatosis (nonmalignant)
NM;NEMALINE CEROID MYOPATHY;NONE;G71.2
O.NEURITIS;OPTIC NEURITIS;H46;Optic neuritis
PYRIDOXINE;PYRIDOXINE DEFICIENCY;E53.1;Pyridoxine deficiency
P.CONGENIT;PARAMYOTONIA CONGENITA;G71.1;Myotonic disorders
PD;PARKINSON DISEASE;G20;Parkinson disease
PLM;PELIZAEUS-MERZBACHER DISEASE;E75.2;Other sphingolipidosis
PMD;PROGRESSIVE MUSCULAR DYSTROPHY;NONE;E75.2
POMPE;POMPE;E74.0;Glycogen storage disease
REFSUM;REFSUM DISEASE;G60.1;Refsum disease
RETT;RETT SYNDROME;F84.2;Rett syndrome
CA;CEREBELLAR ATAXIA;G11.1;Early-onset cerebellar ataxia
SANDHOFF;SANDHOFF DISEASE;E75.0;GM2 gangliosidosis
SCOLIOSIS;SCOLIOSIS;M41;Scoliosis
SCHIZOPHRENIA;SCHIZOPHRENIA;F20;Schizophrenia
SMA;SPINAL MUSCULAR ATROPHY;G12.0;Infantile spinal muscular atrophy, type I [Werdnig-Hoffman]
SMARD;SMARD;NONE;G12.2
TANGIER;TANGIER DISEASE;E78.6;Lipoprotein deficiency
TAY SACHS;TAY SACHS DISEASE;E75.0;GM2 gangliosidosis
TLE;TEMPORAL LOBE EPILEPSY;G40.1;Localization-related (focal)(partial) symptomatic epilepsy and epileptic syndromes with simple partial seizures
TOURETTE;TOURETTE SYNDROME;F95.2;Combined vocal and multiple motor tic disorder [de la Tourette]
ULD;UNVERRICHT-LUNDBORG DISEASE;G40.3;Generalized idiopathic epilepsy and epileptic syndromes
WWS;WALKER-WARBURG SYNDROME;NONE;Q04.3
ND;NEURODEGENERATIVE DISORDER;G30;Alzheimer disease
FAHR;FAHR DISEASE;G23.8;Other specified degenerative diseases of basal ganglia
FR.X;FRAGILE-X SYNDROME;Q99.2;Fragile X chromosome
SLS;SJOGREN-LARSSON SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
SSPE;SUBACUTE SCLEROSING PANENCEPHALITIS;A81.1;Subacute sclerosing panencephalitis
SLO;SMITH LEMLI OPITZ SYNDROME;Q87.1;Congenital malformation syndromes predominantly associated with short stature
C.PALSY;CEREBRAL PALSY;G80;Cerebral palsy
DYSLEXIA;DYSLEXIA;R48.0;Dyslexia and alexia
E-LAMBERT;EATON-LAMBERT SYNDROME;G73.1;Lambert-Eaton syndrome
H.COPROP;HEREDITARY COPROPORPHYRIA;E80.2;Other porphyria
BIPOLAR;BIPOLAR;F31;Bipolar affective disorder
PMA;PERONEAL MUSCULAR ATROPHY;G60.0;Hereditary motor and sensory neuropathy
NB;NEUROBLASTOMA;C74.9;Malignant neoplasm: Adrenal gland, unspecified
MENKES;MENKES SYNDROME;E83.0;Disorders of copper metabolism
WEST;WEST SYNDROME;G40.4;Other generalized epilepsy and epileptic syndromes
ASD;ATRIAL SEPTAL DEFECT;Q21.1;Atrial septal defect
CHD;CONGENITAL HEART DEFECT;Q20;Congenital malformations of cardiac chambers and connections
CMP;CARDIOMYOPATHY;I42.0;Dilated cardiomyopathy
RCMP;RESTRICTIVE CARDIOMYOPATHY;I42.5;Other restrictive cardiomyopathy
POLYMORPHIC VT;PVT;I47.2;Ventricular tachycardia
QTS;LONG QT SYNDROME;I45.8;Other specified conduction disorders
FABRY;FABRY;E75.2;Other sphingolipidosis
GLAUCOMA;GLAUCOMA;H40;Glaucoma
KNIEST;KNIEST SYNDROME;Q77.8;Other osteochondrodysplasia with defects of growth of tubular bones and spine
RB;RETINOBLASTOMA;C69.2;Malignant neoplasm: Retina
RETINOPAT.;RETINOPATHY;H35;Other retinal disorders
GORLIN;GORLIN SYNDROME;Q87.0;Congenital malformation syndromes predominantly affecting facial appearance
PCOS;PCOS;E28.2;Polycystic ovarian syndrome
ACNE;ACNE;L70.0;Acne vulgaris
DC;DYSKERATOSIS CONGENITA;Q82.8;Other specified congenital malformations of skin
ICHTHYOSIS;ICHTHYOSIS;Q80;Congenital ichthyosis
LIPOID PROTEINOSIS;LP;E78.8;Other disorders of lipoprotein metabolism
EB;EPIDERMOLYSIS BULLOSA;Q81;Epidermolysis bullosa
DIGEORGE;DIGEORGE SYNDROME;D82.1;Di George syndrome
E.DYSPLASI;ECTODERMAL DYSPLASIA;Q82.4;Ectodermal dysplasia (anhidrotic)
HC;HEMOCHROMATOSIS;R79.0;Abnormal level of blood mineral
VGD;VON GIERKE DISEASE;E74.0;Glycogen storage disease
MASTOCYTOS;MASTOCYTOSIS;Q82.2;Mastocytosis
MELANOMA;MALIGNANT MELANOMA;C43;Malignant melanoma of skin
HUS;HEMOLYTIC UREMIC SYNDROME;D59.3;Haemolytic-uraemic syndrome
ICHTHYOSIS;ICHTHYOSIS;Q80;Congenital ichthyosis
XP;XERODERMA PIGMENTOSUM;Q82.1;Xeroderma pigmentosum