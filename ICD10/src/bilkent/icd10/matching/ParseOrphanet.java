package bilkent.icd10.matching;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * read orpha db and get list of it
 * it uses sax parser to parse xml
 * 
 * 
 * @author ahmetkucuk
 *
 */
public class ParseOrphanet {

    public static List<OrphaDisease> getOrphaList()
            throws ParserConfigurationException, SAXException,
            MalformedURLException, IOException {

        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        SaxHandler handler = new SaxHandler();
        parser.parse(
                new InputSource(new URL(
                        "http://www.orphadata.org/data/xml/en_product1.xml")
                        .openStream()), handler);
        return handler.disList;
    }

    public static class SaxHandler extends DefaultHandler {

        List<OrphaDisease> disList = new ArrayList<>();
        OrphaDisease dis = null;
        String content = null;
        String lastSource = "";

        @Override
        // Triggered when the start of tag is found.
        public void startElement(String uri, String localName, String qName,
                Attributes attributes) throws SAXException {

            switch (qName) {
            // Create a new Employee object when the start tag is found
            case "Disorder":
                dis = new OrphaDisease();
                break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            switch (qName) {
            // Add the employee to list once end tag is found
            case "Disorder":
                if (dis.getIcd() != null)
                    disList.add(dis);
                break;
            // For all other end tags the employee has to be updated.
            case "OrphaNumber":
                dis.setOrphaNumber(Integer.parseInt(content));
                break;
            case "Synonym":
                dis.addSynonym(content);
                break;
            case "Source":
                lastSource = content;
                break;
            case "Name":
                dis.setName(content);
                break;
            case "Reference":
                if (lastSource.equalsIgnoreCase("ICD10"))
                    dis.setIcd(content);
                if (lastSource.equalsIgnoreCase("OMIM"))
                    dis.setMim(content);
                break;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length)
                throws SAXException {
            content = String.copyValueOf(ch, start, length).trim();
        }

    }

    static class OrphaDisease {
        private int orphaNumber;
        private String name;
        private String mim;
        private List<String> synonyms = new ArrayList<>();
        private String icd;
        
        public OrphaDisease() {
            icd = "NONE";
        }

        public void addSynonym(String s) {
            this.synonyms.add(s);
        }

        public int getOrphaNumber() {
            return orphaNumber;
        }

        public void setOrphaNumber(int orphaNumber) {
            this.orphaNumber = orphaNumber;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public List<String> getSynonyms() {
            return synonyms;
        }

        public void setSynonyms(List<String> synonyms) {
            this.synonyms = synonyms;
        }

        public String getIcd() {
            return icd;
        }

        public void setIcd(String icd) {
            this.icd = icd;
        }

        public String toString() {
            return "name: " + name + " List syn: " + synonyms.toString()
                    + " icd: " + icd;
        }
        
        public boolean contains(String s) {
            if(name.equalsIgnoreCase(s)) {
                return true;
            } else {
                for(String temp: synonyms) {
                    if(temp.equalsIgnoreCase(s))
                        return true;
                }
            }
            return false;
        }

        public String getMim() {
            return mim;
        }

        public void setMim(String mim) {
            this.mim = mim;
        }
    }

}
