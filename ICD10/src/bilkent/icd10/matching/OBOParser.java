package bilkent.icd10.matching;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.Attributes;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

/**
 * Using sax parser, we are parsing obo db
 * 
 * @author ahmetkucuk
 *
 */
public class OBOParser {

    public static void main(String[] args) throws MalformedURLException,
            ParserConfigurationException, SAXException, IOException {
        getOboList();
    }

    public static List<OboItem> getOboList()
            throws ParserConfigurationException, SAXException,
            MalformedURLException, IOException {

        SAXParserFactory parserFactor = SAXParserFactory.newInstance();
        SAXParser parser = parserFactor.newSAXParser();
        SaxHandler handler = new SaxHandler();
        parser.parse(
                new InputSource(new URL(
                        "http://www.berkeleybop.org/ontologies/doid.owl")
                        .openStream()), handler);
        System.out.println("List Size: " + handler.itemList.size() + " Total Size: " + handler.numberOfItem);
        return handler.itemList;
    }

    public static class SaxHandler extends DefaultHandler {

        List<OboItem> itemList = new ArrayList<>();
        OboItem item = null;
        String content = null;
        String lastSource = "";
        int numberOfItem = 0;
        
        Map<String, String> icdNameMap;

        public SaxHandler() throws IOException {
            icdNameMap = ExactMatch.getCodeNameICDTuple(0);
        }

        @Override
        // Triggered when the start of tag is found.
        public void startElement(String uri, String localName, String qName,
                Attributes attributes) throws SAXException {

            switch (qName) {
            // Create a new Employee object when the start tag is found
            case "owl:Class":
                item = new OboItem();
                numberOfItem++;
                break;
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName)
                throws SAXException {
            switch (qName) {
            // Add the employee to list once end tag is found
            case "owl:Class":
                if (item.length() > 0) {
//                    List<String> names = item.getNameList();
//                    for (String s : names) {
//                        if (icdNameMap.containsKey(s)) {
//                            item.setIcd(icdNameMap.get(s));
//                            
//                        }
//                    }
                    itemList.add(item);

                }
                break;
            case "oboInOwl:hasExactSynonym":
                item.addItemName(content);
                // System.out.println(content);
                break;
            }
        }

        @Override
        public void characters(char[] ch, int start, int length)
                throws SAXException {
            content = String.copyValueOf(ch, start, length).trim();
        }

    }

    public static class OboItem {
        private List<String> allNames = new ArrayList<>();
        String icd = "";

        public void addItemName(String s) {
            allNames.add(s);
        }

        public int length() {
            return allNames.size();
        }

        public List<String> getNameList() {
            return allNames;
        }

        public void setIcd(String i) {
            this.icd = i;
        }

        public String getIcd() {
            return this.icd;
        }

        public boolean isContains(String s) {

            for (String name : allNames) {
                if (s.equalsIgnoreCase(name))
                    return true;
            }
            return false;
        }
    }

}
