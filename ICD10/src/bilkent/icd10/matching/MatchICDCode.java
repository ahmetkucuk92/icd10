package bilkent.icd10.matching;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import bilkent.icd10.matching.OBOParser.OboItem;
import bilkent.icd10.matching.ParseOrphanet.OrphaDisease;

/**
 * Using some sort of methods we find icd codes of hacettepe list after we get
 * this icd codes, we are searching for corresponding name in icd list
 * 
 * For the ones that we couldn't find icd, we search for other options whihc
 * are;
 * 
 * Orphan disease, we search in this db by name of hacettepe OBO disease, we
 * search in this list if we hit in this db we get all synoyms and search for
 * them in icd since OBO db does not have icd code in default
 * 
 * @author ahmetkucuk
 * 
 */
public class MatchICDCode {

    static Map<String, String> icdCodeAndName;
    static Map<String, String> icdNameAndCode;

    public static void main(String[] args) throws IOException,
            ParserConfigurationException, SAXException {

        final String hacettepeFile = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/hacettepe_codes_new.txt";
        final String hacettepeFileOutput = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/hacettepe_codes_icd_name_orphaned_and_obo_new.txt";

        final InputStream fis = new FileInputStream(hacettepeFile);
        final BufferedReader br = new BufferedReader(new InputStreamReader(fis,
                Charset.forName("UTF-8")));

        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
                hacettepeFileOutput, true)));

        String line;

        final List<Tuple3<String, String, String>> hacettepeListWithCode = new ArrayList<>();

        while ((line = br.readLine()) != null) {
            String[] codeArr = line.split(";");
            if (codeArr.length < 3)
                System.out.println("Alert Length");
            hacettepeListWithCode.add(new Tuple3<String, String, String>(
                    codeArr[0].trim(), codeArr[1].trim(), codeArr[2].trim()));

        }
        fis.close();
        br.close();
        System.out.println("hacettepe Length: " + hacettepeListWithCode.size());

        icdCodeAndName = ExactMatch.getCodeNameICDTuple(1);
        icdNameAndCode = ExactMatch.getCodeNameICDTuple(0);
        List<OrphaDisease> diseaseList = ParseOrphanet.getOrphaList();
        List<OboItem> oboList = OBOParser.getOboList();

        int numberOfSuccess = 0;
        int numberOfUnexpectedFail = 0;
        int numberOfNotFound = 0;
        int count = 0;

        int wikiSuccess = 0;
        int orphaSuccess = 0;
        int oboSuccess = 0;

        int totalSuccess = 0;

        for (Tuple3<String, String, String> t3 : hacettepeListWithCode) {
            // out.println(t3._1 + ";" + t3._2 + ";" + t3._3 + ";NO_ICD");

            boolean didGet = false;
            for (int i = 0; i < 2 && !didGet; i++) {
                String searchFor = "";
                if (i == 0)
                    searchFor = t3._1;
                else if (i == 1)
                    searchFor = t3._2;

//                System.out.println("RESULT FOR " + i);
                boolean isWikiSucceed = false;

                Tuple3<Boolean, String, String> orphaSearchResult = getFromOrphaList(
                        diseaseList, searchFor);
//                orphaSuccess = (orphaSearchResult._1) ? orphaSuccess + 1
//                        : orphaSuccess;
                

//                System.out.println("ORPHA RESULT: " + t3._1 + ";" + t3._2 + ";"
//                        + orphaSearchResult._2 + ";" + orphaSearchResult._3);
                if(orphaSearchResult._1)
                System.out.println("Orpha: " + orphaSearchResult._2 + " Wiki: " + t3._3);

                Tuple3<Boolean, String, String> oboSearchResult = getFromOboList(
                        oboList, searchFor);
//                oboSuccess = (oboSearchResult._1) ? oboSuccess + 1 : oboSuccess;

//                System.out.println("OBO RESULT: " + t3._1 + ";" + t3._2 + ";"
//                        + oboSearchResult._2 + ";" + oboSearchResult._3);

                if (orphaSearchResult._1 || oboSearchResult._1)
                    totalSuccess++;

                if (!orphaSearchResult._1 && !oboSearchResult._1)
                    didGet = false;
                else {
                    if (orphaSearchResult._1) {
                        out.println(t3._1 + ";" + t3._2 + ";"
                                + orphaSearchResult._2 + ";"
                                + orphaSearchResult._3);
                        orphaSuccess++;
                    } else if (oboSearchResult._1) {
                        out.println(t3._1 + ";" + t3._2 + ";"
                                + oboSearchResult._2 + ";" + oboSearchResult._3);
                        oboSuccess++;
                    }
                    didGet = true;
                }

                // if (!t3._3.equalsIgnoreCase("NONE")) {
                // if (icdCodeAndName.containsKey(t3._3)) {
                // System.out.println("WIKIPEDIA RESULT: " + t3._1 + ";"
                // + t3._2 + ";" + t3._3 + ";"
                // + icdCodeAndName.get(t3._3));
                // numberOfSuccess++;
                // didGet = true;
                // } else {
                // System.out.println(t3._1 + ";" + t3._2 + ";" + t3._3
                // + ";NONE_CORRESPOND");
                // numberOfUnexpectedFail++;
                // }
                // } else {
                // boolean isFound = false;
                // for (OrphaDisease d : diseaseList) {
                // if (d.contains(searchFor)) {
                // System.out.println(t3._1 + ";" + t3._2 + ";"
                // + d.getIcd() + ";" + d.getName());
                //
                // numberOfSuccess++;
                // isFound = true;
                // didGet = true;
                // break;
                // }
                // }
                // if (!isFound) {
                //
                // for (OboItem item : oboList) {
                //
                // if (item.isContains(searchFor)) {
                // List<String> synonyms = item.getNameList();
                // System.out.println(++count);
                // for (String s : synonyms) {
                // if (icdNameAndCode.containsKey(s)) {
                // System.out.println(t3._1 + ";" + t3._2
                // + ";" + icdNameAndCode.get(s)
                // + ";" + s);
                //
                // numberOfSuccess++;
                // isFound = true;
                // didGet = true;
                // break;
                // }
                // }
                // }
                // if (isFound)
                // break;
                // }
                // }
                // }
            }
            //
            if (!didGet) {
                if (icdCodeAndName.containsKey(t3._3)) {
                    out.println(t3._1 + ";"
                            + t3._2 + ";" + t3._3 + ";"
                            + icdCodeAndName.get(t3._3));

                    wikiSuccess++;
                    totalSuccess++;
                } else {
                    out.println(t3._1 + ";" + t3._2 + ";" + t3._3 + ";NO_ICD");

                }
            }
        }

        // System.out.println("Success: " + numberOfSuccess
        // + "\nUnexpected Fail Count: " + numberOfUnexpectedFail
        // + "\nNot Found Count: " + (numberOfNotFound));

        System.out.println("Wiki Success: " + wikiSuccess);
        System.out.println("Orpha Success: " + orphaSuccess);
        System.out.println("OBO Success: " + oboSuccess);
        System.out.println("Total Success: " + totalSuccess);

        // System.out.println("Number of Hit: " + count);
        out.close();

    }

    public static Tuple3<Boolean, String, String> getFromOrphaList(
            List<OrphaDisease> listOD, String itemToSearch) {

        boolean isFound = false;
        int numberOfSuccess = 0;
        String icdCode = "NOT_FOUND";
        String icdName = "NOT_FOUND";
        for (OrphaDisease d : listOD) {
            if (d.contains(itemToSearch)) {
                icdCode = d.getIcd();
                icdName = d.getName();
                numberOfSuccess++;
                isFound = true;
            }
        }
//        System.out.println("GET ORPHA: " + numberOfSuccess);
        return new Tuple3<Boolean, String, String>(isFound, icdCode, icdName);
    }

    public static Tuple3<Boolean, String, String> getFromOboList(
            List<OboItem> oboList, String itemToSearchFor) {

        boolean isFound = false;
        String icdCode = "NOT_FOUND";
        String icdName = "NOT_FOUND";
        int numberOfSuccess = 0;
        for (OboItem item : oboList) {

            if (item.isContains(itemToSearchFor)) {
                List<String> synonyms = item.getNameList();
                for (String s : synonyms) {
                    if (icdNameAndCode.containsKey(s)) {
                        icdCode = icdNameAndCode.get(s);
                        icdName = s;
                        isFound = true;
                        numberOfSuccess++;
                        break;
                    }
                }
            }
        }
//        System.out.println("GET OBO: " + numberOfSuccess);
        return new Tuple3<Boolean, String, String>(isFound, icdCode, icdName);
    }

}
