package bilkent.icd10.matching;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;

/**
 * 
 * this class is running exact match between hacettepe list and icd a part from
 * that it provides two different methods which are getting list of icd code and
 * list of hacettepe codes
 * 
 * @author ahmetkucuk
 * 
 * 
 */
public class ExactMatch {

    public static void main(String[] args) throws IOException {

        final String hacettepeFile = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/hacettepe.txt";
        final String icdFile = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/codes.txt";

        final InputStream fis = new FileInputStream(hacettepeFile);
        final BufferedReader br = new BufferedReader(new InputStreamReader(fis,
                Charset.forName("UTF-8")));

        final InputStream fis2 = new FileInputStream(icdFile);
        final BufferedReader br2 = new BufferedReader(new InputStreamReader(
                fis2, Charset.forName("UTF-8")));

        String line;

        final List<Tuple2<String, String>> hacettepeList = new ArrayList<>();
        final List<String> icdList = new ArrayList<>();

        while ((line = br.readLine()) != null) {

            String[] splitted = line.split("\t");
            if (splitted.length > 1) {
                Tuple2<String, String> hacettepeTuple = new Tuple2<String, String>(
                        splitted[0], splitted[1]);
                hacettepeList.add(hacettepeTuple);
            }
        }

        while ((line = br2.readLine()) != null) {

            String[] splitted = line.split(";");
            icdList.add(splitted[8]);
        }

        int numberOfMatch = 0;

        List<Tuple2<String, String>> l = new ArrayList<>();

        for (Tuple2<String, String> t : hacettepeList) {

            for (String s : icdList) {
                // System.out.println(t + " " + s);
                if (t._2.equalsIgnoreCase(s)) {
                    l.add(new Tuple2<String, String>(t._2, s));
                    System.out.println(t._2 + " " + s);
                    numberOfMatch++;
                }
            }
        }
        System.out.println(numberOfMatch);
        fis.close();
        fis2.close();
        br.close();
        br2.close();
    }

    public static List<Tuple2<String, String>> getHacettepeList()
            throws IOException {

        final String hacettepeFile = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/hacettepe.txt";

        final InputStream fis = new FileInputStream(hacettepeFile);
        final BufferedReader br = new BufferedReader(new InputStreamReader(fis,
                Charset.forName("UTF-8")));

        String line;

        final List<Tuple2<String, String>> hacettepeList = new ArrayList<>();

        while ((line = br.readLine()) != null) {

            String[] splitted = line.split("\t");
            if (splitted.length > 1) {
                Tuple2<String, String> hacettepeTuple = new Tuple2<String, String>(
                        splitted[0], splitted[1]);
                hacettepeList.add(hacettepeTuple);
            }
        }
        System.out.println("Hacettepe original List length: "
                + hacettepeList.size());
        fis.close();
        br.close();
        return hacettepeList;
    }

    public static Map<String, String> getCodeNameICDTuple(int mapDirection)
            throws IOException {

        final String hacettepeFile = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/codes.txt";

        final InputStream fis = new FileInputStream(hacettepeFile);
        final BufferedReader br = new BufferedReader(new InputStreamReader(fis,
                Charset.forName("UTF-8")));

        String line;

        Map<String, String> icdMap = new HashMap<String, String>();

        while ((line = br.readLine()) != null) {

            String[] splitted = line.split(";");
            if (mapDirection == 0) {
                icdMap.put(splitted[8], splitted[6]);
            } else {
                icdMap.put(splitted[6], splitted[8]);
            }
        }
        fis.close();
        return icdMap;
    }

    public static List<DesiredDiseaseFormat> getHacettepeMathcedList()
            throws IOException {

        final String hacettepeFile = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/hacettepe_codes_icd_name_orphaned_and_obo_new.txt";

        final InputStream fis = new FileInputStream(hacettepeFile);
        final BufferedReader br = new BufferedReader(new InputStreamReader(fis,
                Charset.forName("UTF-8")));

        String line;

        List<DesiredDiseaseFormat> list = new ArrayList<>();

        while ((line = br.readLine()) != null) {

            String[] splitted = line.split(";");
            DesiredDiseaseFormat dis = new DesiredDiseaseFormat();
            dis.setHacettepeCode(splitted[0]);
            dis.setHacettepeName(splitted[1]);
            dis.setIcd10(splitted[2]);
            dis.setIcd10Name(splitted[3]);
            list.add(dis);
        }
        fis.close();
        return list;
    }

    public static class DesiredDiseaseFormat {

        private String hacettepeCode;
        private String hacettepeName;
        private String icd10;
        private String icd10Name;
        private String mimNumber;
        private String mimName;
        private String orphaNumber;
        private String orphaName;

        public String toString() {
            // TODO Auto-generated method stub
            String result = hacettepeCode + ";" + hacettepeName + ";" + icd10
                    + ";" + icd10Name + ";" + orphaNumber + ";" + orphaName
                    + ";" + mimNumber + ";" + mimName;
            return result;
        }

        public String getHacettepeCode() {
            return hacettepeCode;
        }

        public void setHacettepeCode(String hacettepeCode) {
            this.hacettepeCode = hacettepeCode;
        }

        public String getHacettepeName() {
            return hacettepeName;
        }

        public void setHacettepeName(String hacettepeName) {
            this.hacettepeName = hacettepeName;
        }

        public String getIcd10() {
            return icd10;
        }

        public void setIcd10(String icd10) {
            this.icd10 = icd10;
        }

        public String getIcd10Name() {
            return icd10Name;
        }

        public void setIcd10Name(String icd10Name) {
            this.icd10Name = icd10Name;
        }

        public String getMimNumber() {
            return mimNumber;
        }

        public void setMimNumber(String mimNumber) {
            this.mimNumber = mimNumber;
        }

        public String getMimName() {
            return mimName;
        }

        public void setMimName(String mimName) {
            this.mimName = mimName;
        }

        public String getOrphaNumber() {
            return orphaNumber;
        }

        public void setOrphaNumber(String orphaNumber) {
            this.orphaNumber = orphaNumber;
        }

        public String getOrphaName() {
            return orphaName;
        }

        public void setOrphaName(String orphaName) {
            this.orphaName = orphaName;
        }

    }

}
