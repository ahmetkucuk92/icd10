package bilkent.icd10.matching;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import bilkent.icd10.matching.ExactMatch.DesiredDiseaseFormat;
import bilkent.icd10.matching.ParseOrphanet.OrphaDisease;

public class AddMIMAndOrpha {

    public static void main(String[] args) throws Exception {

        String hacettepeFileOutput = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/hacettepe_codes_icd_name_orphaned_and_obo_omim_mimname_new.txt";
        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
                hacettepeFileOutput, true)));

        List<OrphaDisease> orphaDiseaseList = ParseOrphanet.getOrphaList();
        List<DesiredDiseaseFormat> desiredDiseases = ExactMatch
                .getHacettepeMathcedList();

        int hitCount = 0;
        int realHitCount = 0;
        for (DesiredDiseaseFormat d : desiredDiseases) {

            for (OrphaDisease o : orphaDiseaseList) {
                if (!d.getIcd10().equalsIgnoreCase("NONE")
                        && d.getIcd10().equalsIgnoreCase(o.getIcd())) {
                    // System.out.println(d.getIcd10() + " " + o.getIcd());
                    if (!(o.getMim() + "").equalsIgnoreCase("null"))
                        realHitCount++;
                    d.setMimNumber(o.getMim());
                    d.setOrphaNumber(o.getOrphaNumber() + "");
                    d.setOrphaName(o.getName());
                    if (o.getMim() != null) {
                        String temp = getMimNames(o.getMim());
                        System.out.println("1: " + temp);
                        if(temp.indexOf(";") != -1) {
                            temp = temp.substring(0, temp.indexOf(";"));
                            System.out.println("2: " + temp);
                        }
                        d.setMimName(temp);
                    }
                    hitCount++;
                    break;
                }
            }
        }
        System.out.println("Hit Count: " + hitCount + " Real Hit Count: "
                + realHitCount);
        for (DesiredDiseaseFormat d : desiredDiseases) {
            out.println(d.toString());
        }
        out.close();

    }

    public static String getMimNames(String mimNumber) throws Exception {

        String result = null;

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        URL url = new URL(
                "http://api.europe.omim.org/api/entry?format=xml&apiKey=F9BC508B683F82898585BC1F61CDC3F7D150B625&mimNumber="
                        + mimNumber);
        URLConnection connection = url.openConnection();

        Document doc = parseXML(connection.getInputStream());
        NodeList descNodes = doc.getElementsByTagName("preferredTitle");
        if (descNodes.getLength() == 1) {
            result = descNodes.item(0).getTextContent();
        } else {
            System.out.println("Something wrong with getMimNames, "
                    + " Length of nodes: " + descNodes.getLength() + " mim: "
                    + mimNumber);
        }

        return result;
    }

    private static Document parseXML(InputStream stream) throws Exception {
        DocumentBuilderFactory objDocumentBuilderFactory = null;
        DocumentBuilder objDocumentBuilder = null;
        Document doc = null;
        try {
            objDocumentBuilderFactory = DocumentBuilderFactory.newInstance();
            objDocumentBuilder = objDocumentBuilderFactory.newDocumentBuilder();

            doc = objDocumentBuilder.parse(stream);
        } catch (Exception ex) {
            throw ex;
        }
        stream.close();
        return doc;
    }

}
