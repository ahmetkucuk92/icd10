package bilkent.icd10.matching;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;


/**
 * this class use basic search on wikipedia to get icd codes
 * if we find icd in corresponding html we get it and write to 
 * a file to use it later.
 * 
 * 48 hit
 * 
 * @author ahmetkucuk
 *
 */
public class WikiPediaSearch {

    public static final String baseWikiSeach = "http://en.wikipedia.org/w/index.php?search=";

    public static void main(String[] args) throws IOException {

        final String outputFileName1 = "/Users/ahmetkucuk/Documents/workspace/Bilkent_ICD10/ICD10/resource/hacettepe_codes_new.txt";

        PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(
                outputFileName1, true)));

        // Elements newsHeadlines = doc.select("#mp-itn b a");

        List<Tuple2<String, String>> listHacettepe = ExactMatch
                .getHacettepeList();

        int numberOfHit = 0;
        int numberOfLine = 0;
        for (Tuple2<String, String> t : listHacettepe) {
            numberOfLine++;
            String code;
            String diseaseName = "";
            if(t._2.length() < t._1.length()) {
                diseaseName = t._1;
            } else {
                diseaseName = t._2;
            }
            code = getCodeForDisease(diseaseName);
            String [] listOfAppendableWords = {"Deficiency", "Disorder", "Disease", "Syndrome"};
            for(int i = 0; i < listOfAppendableWords.length && code.equalsIgnoreCase("NONE"); i++) {
                code = getCodeForDisease(diseaseName + " " + listOfAppendableWords[i]);
            }
            
            if (!code.equalsIgnoreCase("NONE")) {
                numberOfHit++;                
            }
            
            out.println(t._1 + " ; " + t._2 + ";" + code);
            if (numberOfLine % 50 == 0) {
                System.out.println("Line Number: " + numberOfLine
                        + " Current Hit: " + numberOfHit);
            }
        }
        System.out.println("Finished! Hit count: " + numberOfHit + " Number of Line: " + numberOfLine);
        out.close();
    }

    public static String getCodeForDisease(String diseaseName)
            throws IOException {
        Document doc = null;
        try {

            doc = Jsoup.connect(
                    "http://en.wikipedia.org/w/index.php?search=" + diseaseName).timeout(10*1000).get();
        } catch (Exception e) {
            // TODO: handle exception
            System.out.println(e);
        }
        String allHtml = "";
        if(doc != null)
            allHtml = doc.toString();
        String splitString = "http://apps.who.int/classifications/icd10/browse/2010/en#/";
        int index = allHtml
                .indexOf("http://apps.who.int/classifications/icd10/browse/2010/en#/");
        if (index < 0) {
            return "NONE";
        }
        index += splitString.length();
        String result = allHtml.substring(index, index + 5);
        if(result.indexOf("\">") != -1) {
            //System.out.println(code);
            result = result.substring(0, result.indexOf("\">"));
            //System.out.println(code);
        }
        return result;

    }

}
