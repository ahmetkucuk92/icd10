package bilkent.icd10.matching;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;

import org.xml.sax.SAXException;

import bilkent.icd10.matching.ParseOrphanet.OrphaDisease;

/**
 * class to check how many disease we hit in orphanet
 * it is 150
 * @author ahmetkucuk
 *
 */
public class OrphanetSynoynmSearch {
    
    public static void main(String [] args) throws MalformedURLException, ParserConfigurationException, SAXException, IOException {
        
        List<OrphaDisease> diseaseList = ParseOrphanet.getOrphaList();
        
        List<Tuple2<String, String>> listHacettepe = ExactMatch
                .getHacettepeList();
        
        int count = 0;
        for(Tuple2<String, String> t : listHacettepe) {
            
            for(OrphaDisease d: diseaseList) {
                if(d.contains(t._2))
                    count++;
            }
        }
        System.out.println("Number of Hit: " + count);
        
        
        
    }

}
